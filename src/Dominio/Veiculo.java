/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author rafael
 */
public class Veiculo {

    private String placa;
    private String marca;
    private String modelo;
    private int ano;
    private int capacidade;
    private Cor cor;
    private Campus campuOrig;
    private int quilometragem;
    private boolean ativo;
    private CategoriaCNH categoria_cnh;

   

    public Veiculo(String p, String marca, String model, int ano, int capac, 
            Cor cor, Campus idCamp, int quilom, CategoriaCNH cat_cnh) {

        this.placa = p;
        this.marca = marca;
        this.modelo = model;
        this.ano = ano;
        this.capacidade = capac;
        this.cor = cor;
        this.campuOrig = idCamp;
        this.quilometragem = quilom;
        this.categoria_cnh = cat_cnh;

    }

    /**
     * Construtor usado quando os dados estão vindo do bd, ou seja, na hora da
     * criação de uma lista
     *
     * @param p
     * @param marca
     * @param model
     * @param ano
     * @param capac
     * @param cor
     * @param camp
     * @param quilom
     */
    public Veiculo(String p, String marca, String model, int ano, 
            int capac, String cor, String camp, int quilom, CategoriaCNH cat_cnh) {

        this.placa = p;
        this.marca = marca;
        this.modelo = model;
        this.ano = ano;
        this.capacidade = capac;
        //this.cor = Cor.valueOf(cor);
        this.cor = Cor.fromString(cor);
        this.campuOrig = Campus.fromString(camp);
        this.quilometragem = quilom;
        this.categoria_cnh = cat_cnh;

    }

    /**
     * construtor que possui somente o abributo placa, utilizado por exemplo na desativação de um veiculo 
     * @param placa
     */
    public Veiculo(String p) {

        this.placa = p;
    }


    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the capacidade
     */
    public int getCapacidade() {
        return capacidade;
    }

    /**
     * @param capacidade the capacidade to set
     */
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    /**
     * @return the cor
     */
    public Cor getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(Cor cor) {
        this.cor = cor;
    }

    /**
     * @return the campuOrig
     */
    public Campus getCampuOrig() {
        return campuOrig;
    }

    /**
     * @param campuOrig the campuOrig to set
     */
    public void setCampuOrig(Campus campuOrig) {
        this.campuOrig = campuOrig;
    }

    /**
     * @return the quilometragem
     */
    public int getQuilometragem() {
        return quilometragem;
    }

    /**
     * @param quilometragem the quilometragem to set
     */
    public void setQuilometragem(int quilometragem) {
        this.quilometragem = quilometragem;
    }

    /**
     * @return the status
     */
    public boolean getAtivo() {
        return ativo;
    }

    /**
     * @param status the status to set
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;

    }

    /**
     * @return the categoria_cnh
     */
    public CategoriaCNH getCategoria_cnh() {
        return categoria_cnh;
    }

    /**
     * @param categoria_cnh the categoria_cnh to set
     */
    public void setCategoria_cnh(CategoriaCNH categoria_cnh) {
        this.categoria_cnh = categoria_cnh;
    }
}
