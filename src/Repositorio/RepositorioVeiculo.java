/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Dominio.Campus;
import Dominio.Veiculo;
import Dominio.ViagemFretada;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import persistencia.DBVeiculo;
import persistencia.VeiculoDAO;

/**
 *
 * @author Ricardo Burg Machado
 */
public class RepositorioVeiculo {

    private VeiculoDAO vcDAO;

    /**
     * Construtor
     * @param veiculoDAO 
     */
    public RepositorioVeiculo(VeiculoDAO veiculoDAO) {

        this.vcDAO = veiculoDAO;
    }

    /**
     * Método que possibilita armazenar o registro de um veiculo contendo os seguintes dados:
     * placa, marca, modelo, ano, capacidade, cor, quilometragem, campus de origem
     * @param veiculo
     * @return true caso o veiculo tenha sido inserido no BD
     * São feitas as seguintes verificações: objeto null, placa válida (RRN1), placa existente(RNN2), ano(RNN3),
     * capacidade(RNN4), quilometragem(RNN6)
     * Observação: as verificações especificadas no documento de requisitos (RRN5 e RRN7) verificação de Cor e Campus
     * estão sendo atendidas pelo fato de ambas serem classes do tipo Enum
     */
    public boolean incluir(Veiculo veiculo) {

        if (veiculo != null ){
            
            if( !verificaPlacaExistente(veiculo.getPlaca())
                && verificaAnoValido(veiculo.getAno())
                && verificaCapacValida(veiculo.getCapacidade())
                && verificaQuilometragem(veiculo.getQuilometragem())) {
                
                return vcDAO.incluir(veiculo);
            }else{
                System.out.println("RETORNO FALSE -> não foi efetivada a inclusão do veiculo");
                return false;
            }
        } else {
            System.out.println("RETORNO FALSE -> não foi efetivada a inclusão do veiculo (objeto null)");
            return false;
        }
    }

    /**
     * Método que possibilita a alteração dos seguintes dados de um veiculo cadastrado no bd: 
     * marca, modelo, ano, capacidade, cor, quilometragem, campus de origem
     * São feitas as seguintes verificações: objeto null, ano, capacidade, quilometragem
     * @param veiculo
     * @return true caso o veicula seja alterado no BD
     * Estão sendo feitas as seguintes verificações: objeto null, ano(RNN3), capacidade(RNN4), quilometragem(RNN6)
     * Observação: as verificações do documento de requisito (RRN5 e RRN7) verificação de Cor e Campus
     * estão sendo atendidas pelo fato de ambas serem classes do tipo Enum
     */
    public boolean alterar(Veiculo veiculo) {
        if (veiculo != null){
            
            if(verificaAnoValido(veiculo.getAno())&& verificaCapacValida(veiculo.getCapacidade()) 
                    && verificaQuilometragem(veiculo.getQuilometragem())){
           
                return vcDAO.alterar(veiculo);
            }else{
                System.out.println("RETORNO FALSE -> não foi efetivada a alteração do veiculo");
                return false;
            }
        } else {
            System.out.println("RETORNO FALSE -> não foi efetivada a alteração do veiculo(objeto == null)");
            return false;
        }
    }

    /**
     * Método que permite desativar um veiculo cadastrado no banco de dados, ou seja, 
     * a coluna ativo da tabela veiculo é alterado para 'false'
     * @param veiculo
     * @return true se a desativação foi efetivada com sucesso
     * Estão sendo feitas as seguintes verificações: objeto null, se o veiculo possui alguma viagem pendente(RNN9), 
     * placa valida(RNN5)
     */
    public boolean desativar(Veiculo veiculo) {

        if (veiculo != null ){
            if(!verifVeiculoViagemPend(veiculo.getPlaca())&& verificaPlacaValida(veiculo.getPlaca())) {
                return vcDAO.desativar(veiculo);
            }else{
                System.out.println("RETORNO FALSE -> não foi efetivada a desativação do veiculo");
                return false;
            }    
        } else {
            System.out.println("RETORNO FALSE -> não foi efetivada a desativação do veiculo (objeto null)");
            return false;
        }
    }
    
    /**
     * Método que permite a listagem de todos os veiculos cadastrados no banco de dados que estejam ativos
     * 
     */
    public Collection<Veiculo> listar(Campus campus) {

        return vcDAO.listar(campus);
    }

    /**
     * Método que atende a regra de negócio RRN2
     * @param placa
     * @return true caso encontre alguma placa ou a placa passada por parametro seja inválida
     * Estão sendo feitas as seguintes verificações: placa valida, e verifica 
     * se o método consultaplaca retorna um valor maior que zero
     */
    private boolean verificaPlacaExistente(String placa) {

        if (!verificaPlacaValida(placa) || vcDAO.consultaPlaca(placa) > 0) {
            System.out.println("RETORNO TRUE -> placa já existente ou é inválida");
            return true;
        } else {
            System.out.println("RETORNO FALSE -> placa não existente no BD");
            return false;
        }
    }

    /**
     * Método que atende a regra de negócio RRN1
     * @param placa
     * @return true caso a placa seja válida
     */
    private boolean verificaPlacaValida(String placa) {

        Pattern p = Pattern.compile("[A-Z]{3}-\\d{4}");
        //Match the given string with the pattern  
        Matcher m = p.matcher(placa);

        //check whether match is found   
        boolean matchFound = m.matches();

        if (matchFound) {
            System.out.println("RETORNO TRUE -> PLACA VÁLIDA");
            return true;
        } else {
            System.out.println("RETORNO FALSE -> PLACA INVÁLIDA");
            return false;
        }
    }

    /**
     * Método que atende a regra de negócio RNN3, sendo que é verificado se o 
     * valor está no padrão da expressão regular e se o mesmo é maior ou igual que 1950
     * @param ano
     * @return true se o ano for válido
     */
    private boolean verificaAnoValido(int ano) {

        String anoString = String.valueOf(ano);

        Pattern p = Pattern.compile("\\d{4}");
        //Match the given string with the pattern  
        Matcher m = p.matcher(anoString);

        //check whether match is found   
        boolean matchFound = m.matches();

        if (matchFound) {
            if (ano >= 1950) {
                System.out.println("RETORNO TRUE -> ano válido");
                return true;
            } else {
                System.out.println("RETORNO FALSE -> ano inválido");
                return false;
            }
        } else {
            System.out.println("RETORNO FALSE -> ano inválido");
            return false;
        }
    }

    /**
     * Método que atende RNN4, sendo que é verificado se o 
     * valor está no padrão da expressão regular e se o mesmo é maior  que 2
     * @param capac
     * @return true se a capacidade for válida
     */
    private boolean verificaCapacValida(int capac) {

        String capacString = String.valueOf(capac);

        Pattern p = Pattern.compile("\\d{1,}");//tem que ter no mínimo 1 número e somente números
        //Match the given string with the pattern  
        Matcher m = p.matcher(capacString);
        //check whether match is found   
        boolean matchFound = m.matches();

        if (matchFound) {
            if (capac > 2) {
                System.out.println("RETORNO TRUE -> capacidade válida");
                return true;
            } else {
                System.out.println("RETORNO FALSE -> capacidade inválida");
                return false;
            }
        } else {
            System.out.println("RETORNO FALSE -> capacidade inválida");
            return false;
        }
    }

    /**
     * Método que atende a regra de negócio RRN6, sendo que é verificado se o 
     * valor está no padrão da expressão regular e se o mesmo é maior  que 0
     * @param quilometragem
     * @return true se a quilometragem for válida
     */
    private boolean verificaQuilometragem(int quil) {

        String quilString = String.valueOf(quil);

        Pattern p = Pattern.compile("\\d{1,}");//tem que ter no mínimo 1 número e somente números
        //Match the given string with the pattern  
        Matcher m = p.matcher(quilString);
        //check whether match is found   
        boolean matchFound = m.matches();

        if (matchFound) {
            if (quil > 0) {
                System.out.println("RETORNO TRUE -> quilometragem válida");
                return true;
            } else {
                System.out.println("RETORNO FALSE -> quilometragem inválida");
                return false;
            }
        } else {
            System.out.println("RETORNO FALSE -> quilometragem inválida");            
            return false;
        }
    }

    /**
     * Método que atende a regra de negócio RRN9, se que é verificado se a placa é válida, e é verificado
     * o retorno de quantidade de viagens pendentes encontradas no banco de dados
     * @param placa
     * @return true caso haja pelo menos uma viagem pendente do veiculo identificado pela placa
     */
    private boolean verifVeiculoViagemPend(String placa) {

        if (verificaPlacaValida(placa) && vcDAO.buscaQuantProxViagens(placa) > 0) {
            System.out.println("EXISTE UMA OU MAIS VIAGENS PENDENTES");
            return true;
        } else {
            System.out.println("NÃO EXISTE NENHUMA VIAGEM PENDENTE");
            return false;
        }
    }
    
    /**
     * Método que atende o requisito funcional RFF3, sendo que é verificado se a placa é válida
     * @param placa
     * @return Veiculo
     */
    public Veiculo buscaVeiculo(String placa) {

        if( verificaPlacaValida(placa)){
               return vcDAO.buscaVeiculo(placa);
        }else{
            return null;
        }
    }
    
    
}
