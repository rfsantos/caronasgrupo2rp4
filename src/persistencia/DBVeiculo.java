/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.CategoriaCNH;
import Dominio.Cor;
import Dominio.Veiculo;
import Dominio.ViagemFretada;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.omg.CORBA.NameValuePair;

/**
 *
 * @author Ricardo Burg Machado
 */
public class DBVeiculo implements VeiculoDAO {

    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private static final String TABELA = "veiculo";

    public DBVeiculo(Connection connection) {

        this.connection = connection;
    }

    @Override
    public boolean incluir(Veiculo v) {
        try {
            statement = this.connection.createStatement();

            String query = "INSERT INTO " + TABELA + " "
                    + "(placa, marca, modelo, ano, capacidade, quilometragem, campus, cor, ativo, categoria_cnh)"
                    + " VALUES ("
                    + "'" + v.getPlaca() + "',"
                    + "'" + v.getMarca() + "',"
                    + "'" + v.getModelo() + "',"
                    + "" + v.getAno() + ","
                    + "" + v.getCapacidade() + ","
                    + "" + v.getQuilometragem() + ","
                    + "'" + v.getCampuOrig() + "',"
                    + "'" + v.getCor() + "',"
                    + "'TRUE',"
                    + "'" + v.getCategoria_cnh() + "'"
                    + ")";

            System.out.println("QUERY: " + query);

            int retorno = statement.executeUpdate(query.toString());

            if (retorno == 0) {
                System.out.println("RETORNO FALSE -> BD o veiculo não foi incluido no banco");
                return false;
            } else {
                System.out.println("RETORNO TRUE -> BD o veiculo foi incluido com sucesso no banco de dados");
                return true;
            }

        } catch (SQLException ex) {
            System.out.println("RETORNO FALSE -> BD o veiculo não foi incluido no banco");
            System.out.println("EXCEPTION: " + ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean alterar(Veiculo v) {

        try {
            statement = this.connection.createStatement();

            StringBuilder query = new StringBuilder();
            query.append("UPDATE " + TABELA + " SET ");
            query.append("marca = '" + v.getMarca() + "'");
            query.append(" , modelo = '" + v.getModelo() + "'");
            query.append(" , ano = " + v.getAno());
            query.append(" , capacidade = " + v.getCapacidade());
            query.append(" , quilometragem = " + v.getQuilometragem());
            query.append(" , campus = '" + v.getCampuOrig() + "'");
            query.append(" , cor = '" + v.getCor() + "'");
            query.append(" , categoria_cnh = '" + v.getCategoria_cnh() + "'");
            query.append(" WHERE placa = '" + v.getPlaca() + "'");

            System.out.println("QUERY: " + query);

            int retorno = statement.executeUpdate(query.toString());

            System.out.println("RETORNO BD: " + retorno);

            if (retorno == 0) {
                System.out.println("RETORNO FALSE -> BD o veiculo não foi alterado no banco");
                return false;
            } else {
                System.out.println("RETORNO TRUE -> BD o veiculo foi alterado com sucesso no banco de dados");
                return true;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }



    }

    @Override
    public boolean desativar(Veiculo v) {

        try {
            statement = this.connection.createStatement();

            StringBuilder query = new StringBuilder();
            query.append("UPDATE " + TABELA + " SET ");

            query.append(" ativo = 'FALSE' ");

            query.append(" WHERE placa = '" + v.getPlaca() + "'");

            System.out.println("QUERY: " + query);

            int retorno = statement.executeUpdate(query.toString());

            if (retorno == 0) {
                System.out.println("RETORNO FALSE -> BD: o veiculo não foi desativado no banco de dados");
                return false;
            } else {
                System.out.println("RETORNO TRUE -> BD: o veiculo foi desativado com sucesso no banco de dados");
                return true;
            }

        } catch (SQLException ex) {
            System.out.println("RETORNO FALSE -> BD: o veiculo não foi desativado no banco de dados");
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public Collection<Veiculo> listar(Campus campus) {

        String query;
        ArrayList<Veiculo> retorno = new ArrayList<>();

        try {
            statement = this.connection.createStatement();

            if (campus == null) {
                query = "SELECT * FROM " + TABELA + " ORDER BY ano asc";
            } else {
                query = "SELECT * FROM " + TABELA + " "
                        + "WHERE campus = '" + String.valueOf(campus).toString() + "' "
                        + " AND ativo = 'TRUE'"
                        + " ORDER BY ano asc";
            }

            System.out.println("QUERY: " + query);

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                retorno.add(
                        new Veiculo(
                        resultSet.getString("placa").toString(), resultSet.getString("marca"),
                        resultSet.getString("modelo"), resultSet.getInt("ano"),
                        resultSet.getInt("capacidade"), resultSet.getString("cor"),
                        resultSet.getString("campus"), resultSet.getInt("quilometragem"),
                        CategoriaCNH.fromString( resultSet.getString("categoria_cnh")) ) );
            }
            return retorno;

        } catch (SQLException ex) {
            System.out.println("EXCEPTION: " + ex);
            return null;
        }
    }

    /**
     * @param placa
     * @return int -> quantidade de placas identicas identicas armazenada na tabela veiculo
     */
    @Override
    public int consultaPlaca(String placa) {

        try {
            statement = this.connection.createStatement();

            String query = "SELECT COUNT(*) FROM " + TABELA + " WHERE placa = '" + placa + "'";

            System.out.println("QUERY: " + query);

            ResultSet resultSet = statement.executeQuery(query);

            resultSet.next();

            int retorno = resultSet.getInt(1);

            System.out.println("QTD PLACA ENCONTRADAS: " + retorno);

            return retorno;
        } catch (SQLException ex) {
            System.out.println("EXCEPTION: " + ex);
            return 1;
        }
    }

    /**
     *
     * @param placa
     * @return int quantidade de viagens (comum) com data marcada igual ou maior do que a data atual, 
     * ou seja, que ainda não foram efetuadas
     */
    @Override
    public int buscaQuantProxViagens(String placa) {

        try {
            statement = this.connection.createStatement();

            String query = "SELECT COUNT(*) FROM viagem_comum "
                    + "WHERE placa = '" + placa + "' "
                    + "AND data_hora_saida >= CURRENT_DATE";

            System.out.println("QUERY: " + query);

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            int retorno = resultSet.getInt(1);

            System.out.println("QTD PROX VIAGENS: " + retorno);
            return retorno;

        } catch (SQLException ex) {
            System.out.println("EXCEPTION: " + ex);
            return 1;
        }
    }

    @Override
    public Veiculo buscaVeiculo(String placa) {
        
        try {
            statement = this.connection.createStatement();
            String query = "SELECT * FROM "+TABELA+ ""
                    + " WHERE placa = '"+placa+"'";

            System.out.println("QUERY: " + query);

            ResultSet rs = statement.executeQuery(query);
            rs.next();
            //('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
            Veiculo v = new Veiculo(rs.getString("placa"), 
                    rs.getString("marca"), rs.getString("modelo"),
                    rs.getInt("ano"), rs.getInt("capacidade"),
                    rs.getString("cor"), rs.getString("campus"), 
                    rs.getInt("quilometragem"), CategoriaCNH.fromString( rs.getString("categoria_cnh")));
            return v;

        } catch (SQLException ex) {
            System.out.println("EXCEPTION : " + ex.getMessage());
            return null;
        }
    }


}
