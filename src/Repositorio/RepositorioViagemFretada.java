/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Aplicacao.PersistenciaFactoryPostgres;
import Dominio.Campus;
import Dominio.Motorista;
import Dominio.Veiculo;
import Dominio.Verificacoes;
import Dominio.Viagem;
import Dominio.ViagemFretada;
import Excecoes.DadoIncorretoException;
import Excecoes.DadoIncorretoException;
import Excecoes.PersistenciaException;
import java.util.Calendar;
import persistencia.ViagemFretadaDAO;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author rafael
 */
public class RepositorioViagemFretada {

    private ViagemFretadaDAO vfDao;

    public RepositorioViagemFretada(ViagemFretadaDAO vfDao) {

        this.vfDao = vfDao;
    }

    public void incluir(ViagemFretada viagem) throws PersistenciaException, DadoIncorretoException {

        if (verificaCompletude(viagem) && verificaAntecedencia(viagem.getDataHoraSaida())
                && verificaDataHoraRetorno(viagem.getDataHoraSaida(), viagem.getDataHoraRetorno(), viagem.getDuracao())
                && verificaCapacidadeVeiculo(viagem.getVeiculo(), viagem.getNumPassageiros())
                && verificaCompatibilidadeCNH(viagem.getVeiculo(), viagem.getMotorista())
                && verificaDestino(viagem.getOrigem(), viagem.getDestino())
                && verificaMotoristaCampus(viagem.getMotorista(), viagem.getOrigem())
                && verificaVeiculoCampus(viagem.getVeiculo(), viagem.getOrigem())
                && verificaCPF(viagem.getCPFResponsavel()) && verificaMotoristaExistente(viagem.getMotorista())
                && verificaMotoristaDisponivel(viagem.getMotorista().getCpf(), viagem.getDataHoraSaida(), viagem.getDuracao())
                && verificaVeiculoExistente(viagem.getVeiculo())
                && verificaVeiculoDisponivel(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagem.getDuracao()) /*&& verificaDistancia(viagem.getDistancia()) && verificaDuracao(viagem.getDuracao())*/) {

            vfDao.incluir(viagem);
        }
    }

    public void alterar(String placaVeiculo, Date dataHoraSaida, ViagemFretada viagem) throws PersistenciaException, DadoIncorretoException, DadoIncorretoException {

        if (dataHoraSaida == null) {

            throw new DadoIncorretoException("Não foi informada a data/hora de saída da viagem para alteração da viagem", dataHoraSaida);
        } else if (placaVeiculo == null) {

            throw new DadoIncorretoException("Não foi informada a placa do veículo para alteração da viagem", this);
        } else {
            if (verificaCompletude(viagem) && verificaAntecedencia(viagem.getDataHoraSaida()) && verificaAntecedencia(dataHoraSaida)
                    && verificaDataHoraRetorno(viagem.getDataHoraSaida(), viagem.getDataHoraRetorno(), viagem.getDuracao())
                    && verificaCapacidadeVeiculo(viagem.getVeiculo(), viagem.getNumPassageiros())
                    && verificaCompatibilidadeCNH(viagem.getVeiculo(), viagem.getMotorista())
                    && verificaDestino(viagem.getOrigem(), viagem.getDestino())
                    && verificaMotoristaCampus(viagem.getMotorista(), viagem.getOrigem())
                    && verificaVeiculoCampus(viagem.getVeiculo(), viagem.getOrigem())
                    && verificaCPF(viagem.getCPFResponsavel()) && verificaMotoristaExistente(viagem.getMotorista())
                    && verificaMotoristaDisponivel(viagem.getMotorista().getCpf(), viagem.getDataHoraSaida(), viagem.getDuracao())
                    && verificaVeiculoExistente(viagem.getVeiculo())
                    && verificaVeiculoDisponivel(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagem.getDuracao()) /*&& verificaDistancia(viagem.getDistancia()) && verificaDuracao(viagem.getDuracao())*/) {

                vfDao.alterar(placaVeiculo, dataHoraSaida, viagem);
            }
        }
    }

    public void excluir(String placaVeiculo, Date dataHoraSaida) throws PersistenciaException, DadoIncorretoException {

        if (dataHoraSaida == null) {

            throw new DadoIncorretoException("Não foi informada a data/hora de saída da viagem para exclusão da viagem", dataHoraSaida);
        } else if (placaVeiculo == null) {

            throw new DadoIncorretoException("Não foi informada a placa do veículo para exclusão da viagem", this);
        } else {

            if (verificaAntecedencia(dataHoraSaida)) {

                vfDao.excluir(placaVeiculo, dataHoraSaida);
            }
        }
    }

    public Collection<ViagemFretada> listar(Date inicioPeriodo, Date fimPeriodo, Campus origem) throws PersistenciaException {

        return vfDao.listar(inicioPeriodo, fimPeriodo, origem);
    }

    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException {

        return vfDao.proximasViagensVeiculo(placaVeiculo);
    }

    private Collection<ViagemFretada> ordenarPorDataHora(Collection<ViagemFretada> viagens) {
        throw new UnsupportedOperationException();
    }

    private boolean verificaCompletude(ViagemFretada viagem) throws DadoIncorretoException {

        if (viagem == null) {

            throw new DadoIncorretoException("Não foi informada a viagem que deve ser incluída", viagem);
        }

        try {

            if (viagem.getMotorista().getCpf() == null || viagem.getMotorista().getCpf().equals("")) {

                throw new DadoIncorretoException("Não foi informado o CPF do motorista", viagem.getMotorista().getCpf());
            } else if (viagem.getMotorista().getCampus() == null) {

                throw new DadoIncorretoException("Não foi informado o campus ao qual o motorista pertence", viagem.getMotorista().getCampus());
            } else if (viagem.getMotorista().getCategoriaCNH() == null) {

                throw new DadoIncorretoException("Não foi informada a categoria da CNH do motorista", viagem.getMotorista().getCategoriaCNH());
            } else if (viagem.getVeiculo().getCapacidade() <= 0) {

                throw new DadoIncorretoException("Não foi informada a capacidade máxima do veículo ou ela não é valida", viagem.getVeiculo().getCapacidade());
            }
        } catch (NullPointerException npe) {

            throw new DadoIncorretoException("Não foi informado o motorista", viagem.getMotorista());
        }

        try {

            if (viagem.getVeiculo().getPlaca() == null) {

                throw new DadoIncorretoException("Não foi informado a placa do veículo", viagem.getVeiculo().getPlaca());
            } else if (viagem.getVeiculo().getCampuOrig() == null) {

                throw new DadoIncorretoException("Não foi informada a cidade a qual o veículo pertence", viagem.getVeiculo().getCampuOrig());
            } else if (viagem.getVeiculo().getCategoria_cnh() == null) {

                throw new DadoIncorretoException("Não foi informada a categoria mínima exigida para a condução do veículo", this);
            }
        } catch (NullPointerException npe) {

            throw new DadoIncorretoException("Não foi informado o veículo", viagem.getVeiculo());
        }

        if (viagem.getOrigem() == null) {

            throw new DadoIncorretoException("Não foi informado o campus de origem da viagem", viagem.getOrigem());
        } else if (viagem.getDistancia() <= 0) {

            throw new DadoIncorretoException("Não foi informada a distância a ser percorrida na viagem ou ela é inválida", viagem.getDistancia());
        } else if (viagem.getDataHoraSaida() == null) {

            throw new DadoIncorretoException("Não foram informadas a data e hora de saída da viagem", viagem.getDataHoraSaida());
        } else if ("".equals(viagem.getCPFResponsavel()) || viagem.getCPFResponsavel() == null) {

            throw new DadoIncorretoException("Não foi informado o CPF do responsável", viagem.getCPFResponsavel());
        } else if (viagem.getResponsavel() == null || "".equals(viagem.getResponsavel())) {

            throw new DadoIncorretoException("Não foi informado o nome do responsável pela viagem", viagem.getResponsavel());
        } else if (viagem.getDataHoraRetorno() == null) {

            throw new DadoIncorretoException("Não foram informadas data e hora de retorno da viagem", viagem.getDataHoraRetorno());
        } else if (viagem.getDestino() == null || "".equals(viagem.getDestino())) {

            throw new DadoIncorretoException("Não foi informado o destino da viagem", viagem.getDestino());
        } else if (viagem.getDuracao() <= 0) {

            throw new DadoIncorretoException("Não foi calculado o tempo de duração da viagem ou é inválido", viagem.getDuracao());
        } else if (viagem.getMotivo() == null || "".equals(viagem.getMotivo())) {

            throw new DadoIncorretoException("Não foi informado o motivo da viagem", viagem.getMotivo());
        } else if (viagem.getNumPassageiros() <= 0) {

            throw new DadoIncorretoException("Não foi informado o número de passageiros na viagem ou não respeita o limite mínimo (1)", viagem.getNumPassageiros());
        }

        return true;
    }

    private boolean verificaVeiculoExistente(Veiculo veiculo) throws DadoIncorretoException {

        if (new PersistenciaFactoryPostgres().createPersistenciaVeiculo().buscaVeiculo(veiculo.getPlaca()) == null) {

            throw new DadoIncorretoException("O veículo informado não consta no sistema", veiculo);
        }
        return true;
    }

    private boolean verificaMotoristaExistente(Motorista motorista) throws DadoIncorretoException {

        if (new PersistenciaFactoryPostgres().createPersistenciaMotorista().buscarMotorista(motorista.getCpf()) == null) {

            throw new DadoIncorretoException("O motorista informado não consta no sistema", motorista);
        }

        return true;
    }

    private boolean verificaVeiculoDisponivel(String placaVeiculo, Date dataHoraSaidaViagem, float duracao) throws PersistenciaException {

        Collection<Viagem> viagens = proximasViagensVeiculo(placaVeiculo);
        viagens.addAll(new PersistenciaFactoryPostgres().createPersistenciaViagemComum().proximasViagensVeiculo(placaVeiculo));

        for (Viagem v : viagens) {

            Calendar c = Calendar.getInstance();
            c.setTime(v.getDataHoraRetorno());
            c.roll(Calendar.HOUR, (int) Math.floor(duracao));
            c.roll(Calendar.MINUTE, (int) ((duracao % Math.floor(duracao)) * 60));

            if (dataHoraSaidaViagem.after(v.getDataHoraSaida()) && dataHoraSaidaViagem.before(v.getDataHoraRetorno())) {

                throw new DadoIncorretoException("O veículo não estará disponível para o período solicitado", placaVeiculo);
            }
        }

        return true;
    }

    private boolean verificaMotoristaDisponivel(String cpfMotorista, Date dataHoraSaidaViagem, float duracao) throws PersistenciaException, DadoIncorretoException {

        Collection<Viagem> viagens = proximasViagensVeiculo(cpfMotorista);
        viagens.addAll(new PersistenciaFactoryPostgres().createPersistenciaViagemComum().proximasViagensVeiculo(cpfMotorista));

        for (Viagem v : viagens) {

            Calendar c = Calendar.getInstance();
            c.setTime(v.getDataHoraRetorno());
            c.roll(Calendar.HOUR, (int) Math.floor(duracao));
            c.roll(Calendar.MINUTE, (int) ((duracao % Math.floor(duracao)) * 60));

            if (dataHoraSaidaViagem.after(v.getDataHoraSaida()) && dataHoraSaidaViagem.before(c.getTime())) {

                throw new DadoIncorretoException("O motorista não estará disponível para o período solicitado", cpfMotorista);
            }
        }

        return true;
    }

    private boolean verificaAntecedencia(Date dataHoraSaida) throws DadoIncorretoException {

        final long DIA = 24 * 60 * 60 * 1000;

        if (((dataHoraSaida.getTime() - new Date().getTime())) / DIA < 1) {

            throw new DadoIncorretoException("Não foi respeitada a regra de no mínimo 1 dia de antecedência para a reserva da viagem", dataHoraSaida);
        }

        return true;
    }

    private boolean verificaDataHoraRetorno(Date dataHoraSaida, Date dataHoraRetorno, float duracao) throws DadoIncorretoException {

        Calendar c = Calendar.getInstance();
        c.setTime(dataHoraSaida);
        c.roll(Calendar.HOUR, (int) Math.floor(duracao));
        c.roll(Calendar.MINUTE, (int) ((duracao % Math.floor(duracao)) * 60));

        if (dataHoraRetorno.before(c.getTime())) {

            throw new DadoIncorretoException("Data/hora de retorno inválidos. A data/hora de retorno devem ser posteriores à data/hora de saída mais o tempo de duração da viagem", dataHoraRetorno);
        }

        return true;
    }

    private boolean verificaCapacidadeVeiculo(Veiculo veiculo, int numPassageiros) throws DadoIncorretoException {

        if (numPassageiros > veiculo.getCapacidade()) {

            throw new DadoIncorretoException("O número de passageiros informado é maior do que a capacidade do veículo", numPassageiros);
        } /*else if (numPassageiros < 0) {

         throw new DadoIncorretoException("Não é possível uma viagem ter menos de 0 passageiros", numPassageiros);
         }*/

        return true;
    }

    private boolean verificaCompatibilidadeCNH(Veiculo veiculo, Motorista motorista) throws DadoIncorretoException {

        if (veiculo.getCategoria_cnh().ordinal() > motorista.getCategoriaCNH().ordinal()) {

            throw new DadoIncorretoException("A categoria de CNH do motorista é incompatível com a categoria de CNH exigida pelo veículo", motorista.getCategoriaCNH());
        }

        return true;
    }

    private boolean verificaDestino(Campus origem, String destino) throws DadoIncorretoException {

        if (origem.toString().equalsIgnoreCase(destino)) {

            throw new DadoIncorretoException("Uma viagem não pode ter origem e destino iguais", destino);
        }

        return true;
    }

    private boolean verificaVeiculoCampus(Veiculo veiculo, Campus campus) throws DadoIncorretoException {

        if (veiculo.getCampuOrig() != campus) {

            throw new DadoIncorretoException("O veículo deve pertencer ao mesmo campus de onde a viagem partirá", veiculo.getCampuOrig());
        }

        return true;
    }

    private boolean verificaMotoristaCampus(Motorista motorista, Campus campus) throws DadoIncorretoException {

        if (motorista.getCampus() != campus) {

            throw new DadoIncorretoException("O veículo deve pertencer ao mesmo campus de onde a viagem partirá", motorista.getCampus());
        }

        return true;
    }

    /*private boolean verificaDistancia(float distancia) throws DadoIncorretoException{
        
     if(distancia < 0){
            
     throw new DadoIncorretoException("A distância informada não é válida (menor do que 0)", distancia);
     }
        
     return true;
     }*/
    /*private boolean verificaDuracao(float duracao)throws DadoIncorretoException{
        
     if(duracao < 0){
            
     throw new DadoIncorretoException("A duração da viagem informada não é válida (menor do que 0)", duracao);
     }
        
     return true;
     }*/
    public static boolean verificaCPF(String CPF) throws DadoIncorretoException {

        //Pattern padrao = Pattern.compile("[0-9]{10}");
        //Matcher verificador = padrao.matcher(CPF);

        if (!Verificacoes.VerCpfValido(CPF)) {

            throw new DadoIncorretoException("O CPF do responsável informado não é válido", CPF);
        }

        return true;
    }
}
