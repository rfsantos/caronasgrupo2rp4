/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

/**
 *
 * @author ricardo
 */
public enum CategoriaCNH {
    A,B,C,D,E,AB,AC,AD,AE;
    
    public static CategoriaCNH fromString(String text) {
        if (text != null) {
            for (CategoriaCNH c : CategoriaCNH.values()) {
                if (text.equalsIgnoreCase(c.toString())) {
                    return c;
                }
            }
        }
        return null;
    }

}
