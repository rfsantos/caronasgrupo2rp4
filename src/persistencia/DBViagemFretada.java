/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.Motorista;
import Dominio.Veiculo;
import Dominio.Viagem;
import Dominio.ViagemFretada;
import Excecoes.PersistenciaException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author rafael
 */
public class DBViagemFretada implements ViagemFretadaDAO {

    private Connection connection;

    public DBViagemFretada(Connection connection) {

        this.connection = connection;
    }

    /*private Time floatToTime(float val) {

     int hora = (int) Math.floor(val);
     int minutos = (int) ((val % hora) * 60);

     return new Time(hora, minutos, 0);
     }*/
    @Override
    public void incluir(ViagemFretada viagem) throws PersistenciaException {

        incluirViagem(viagem);

        String sqlInsertViagemFretada = "insert into viagem_fretada values (?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlInsertViagemFretada);
        } catch (SQLException ex) {
            throw new PersistenciaException("Não foi possível configurar o SQL para inserir Viagem Fretada", ex);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(viagem.getDataHoraSaida().getTime()));
            stmt.setString(2, viagem.getVeiculo().getPlaca());
            stmt.setString(3, viagem.getResponsavel());
            stmt.setInt(4, viagem.getNumPassageiros());
            stmt.setString(5, viagem.getMotivo());
            stmt.setString(6, viagem.getDestino());
            stmt.setString(7, viagem.getCPFResponsavel());
            stmt.setFloat(8, viagem.getDuracao());
            stmt.setTimestamp(9, new java.sql.Timestamp(viagem.getDataHoraRetorno().getTime()));
        } catch (SQLException ex) {

            excluir(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida());
            throw new PersistenciaException("Não foi possível popular o SQL para inserir Viagem Fretada", ex);
        }

        try {

            stmt.executeUpdate();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível inserir a viagem fretada no banco de dados!", sqle);
        }
    }

    private void incluirViagem(Viagem viagem) throws PersistenciaException {

        String sqlInsertViagem = "insert into viagem values (?,?,?,?,?)";
        PreparedStatement stmt;
        try {

            stmt = connection.prepareStatement(sqlInsertViagem);
        } catch (SQLException ex) {
            throw new PersistenciaException("Não foi possível configurar o SQL para inserir Viagem", ex);
        }

        try {

            stmt = connection.prepareStatement(sqlInsertViagem);
        } catch (SQLException ex) {
            throw new PersistenciaException("Não foi possível configurar o SQL para inserir Viagem", ex);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(viagem.getDataHoraSaida().getTime()));
            stmt.setString(2, viagem.getVeiculo().getPlaca());
            stmt.setFloat(3, viagem.getDistancia());
            stmt.setString(4, String.valueOf(viagem.getMotorista().getCpf()));
            stmt.setString(5, viagem.getOrigem().toString());
        } catch (SQLException ex) {

            throw new PersistenciaException("Não foi possível popular o SQL para inserir Viagem", ex);
        }

        try {

            stmt.executeUpdate();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível inserir a viagem no banco de dados!", sqle);
        }
    }

    @Override
    public void alterar(String placaVeiculo, Date dataHoraSaida, ViagemFretada viagem) throws PersistenciaException {

        alterarViagem(placaVeiculo, dataHoraSaida, viagem);
        String sqlAlterarViagemFretada = "update viagem_fretada set responsavel=?, num_passageiros=?, destino=?, cpf_responsavel=?, duracao=?, motivo=?, data_hora_retorno=? where data_hora_saida = ? and placa = ?";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlAlterarViagemFretada);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para alterar viagem fretada", sqle);
        }

        try {

            stmt.setString(1, viagem.getResponsavel());
            stmt.setInt(2, viagem.getNumPassageiros());
            stmt.setString(3, viagem.getDestino());
            stmt.setString(4, viagem.getCPFResponsavel());
            stmt.setFloat(5, viagem.getDuracao());
            stmt.setString(6, viagem.getMotivo());
            stmt.setTimestamp(7, new java.sql.Timestamp(viagem.getDataHoraRetorno().getTime()));
            stmt.setTimestamp(8, new java.sql.Timestamp(viagem.getDataHoraSaida().getTime()));
            stmt.setString(9, viagem.getVeiculo().getPlaca());
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para alterar viagem", sqle);
        }

        try {

            stmt.executeUpdate();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível atualizar a viagem!", sqle);
        }
    }

    private void alterarViagem(String placaVeiculo, Date dataHoraSaida, Viagem viagem) throws PersistenciaException {

        String sqlAlterarViagem = "update viagem set data_hora_saida=?, placa=?, distancia=?, cpf=?, origem=? where data_hora_saida=? and placa=?";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlAlterarViagem);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para alterar viagem", sqle);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(viagem.getDataHoraSaida().getTime()));
            stmt.setString(2, viagem.getVeiculo().getPlaca());
            stmt.setFloat(3, viagem.getDistancia());
            stmt.setString(4, String.valueOf(viagem.getMotorista().getCpf()));
            stmt.setString(5, viagem.getOrigem().toString());
            stmt.setTimestamp(6, new java.sql.Timestamp(dataHoraSaida.getTime()));
            stmt.setString(7, placaVeiculo);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para alterar viagem", sqle);
        }

        try {

            stmt.executeUpdate();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível atualizar a viagem!", sqle);
        }
    }

    @Override
    public void excluir(String placaVeiculo, Date dataHoraSaida) throws PersistenciaException {

        String sqlExcluiViagem = "delete from viagem where data_hora_saida=? and placa=?";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlExcluiViagem);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para deletar viagem", sqle);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(dataHoraSaida.getTime()));
            stmt.setString(2, placaVeiculo);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para deletar viagem", sqle);
        }

        try {

            stmt.executeUpdate();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível deletar a viagem!", sqle);
        }
    }

    @Override
    public Collection<ViagemFretada> listar(Date inicioPeriodo, Date fimPeriodo, Campus origem) throws PersistenciaException {

        String sqlListarViagensFretadas = "select * from viagem natural join viagem_fretada "
                + "where data_hora_saida between ? and ? and origem = ? order by data_hora_saida";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlListarViagensFretadas);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para listar viagens", sqle);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(inicioPeriodo.getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(fimPeriodo.getTime()));
            stmt.setString(3, origem.toString());
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para listar viagens", sqle);
        }

        return performQuery(stmt);
    }

    private Collection<ViagemFretada> listar(Date inicioPeriodo, Date fimPeriodo) throws PersistenciaException {

        String sqlListarViagensFretadas = "select * from viagem natural join viagem_fretada "
                + "where data_hora_saida between ? and ? order by data_hora_saida";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlListarViagensFretadas);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para listar viagens", sqle);
        }

        try {

            stmt.setTimestamp(1, new java.sql.Timestamp(inicioPeriodo.getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(fimPeriodo.getTime()));
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para listar viagens", sqle);
        }

        return performQuery(stmt);
    }

    private Collection<ViagemFretada> listar(Campus origem) throws PersistenciaException {

        String sqlListarViagensFretadas = "select * from viagem natural join viagem_fretada "
                + "where origem=? order by data_hora_saida";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlListarViagensFretadas);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para listar viagens", sqle);
        }

        try {

            stmt.setString(1, origem.toString());
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível popular o sql para listar viagens", sqle);
        }

        return performQuery(stmt);
    }

    private Collection<ViagemFretada> listar() throws PersistenciaException {

        String sqlListarViagensFretadas = "select * from viagem natural join viagem_fretada order by data_hora_saida";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlListarViagensFretadas);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para listar viagens", sqle);
        }

        return performQuery(stmt);
    }

    private Collection<ViagemFretada> performQuery(PreparedStatement stmt) throws PersistenciaException {

        ResultSet result;
        Collection<ViagemFretada> viagens = new LinkedList<>();

        try {

            result = stmt.executeQuery();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível listar as viagens!", sqle);
        }

        try {

            while (result.next()) {

                Motorista motorista = new Motorista();
                motorista.setCpf(result.getString("cpf"));
                Veiculo veiculo = new Veiculo(result.getString("placa"));
                ViagemFretada viagem = new ViagemFretada(veiculo, motorista, Campus.fromString(result.getString("origem")), new Date(result.getTimestamp("data_hora_saida").getTime()), result.getFloat("distancia"));
                viagem.setCPFResponsavel(result.getString("cpf_responsavel"));
                viagem.setDataHoraRetorno(new Date(result.getTimestamp("data_hora_retorno").getTime()));
                viagem.setDuracao(result.getFloat("duracao"));
                viagem.setMotivo(result.getString("motivo"));
                viagem.setResponsavel(result.getString("responsavel"));
                viagem.setNumPassageiros(result.getInt("num_passageiros"));
                viagem.setDestino(result.getString("destino"));

                viagens.add(viagem);
            }
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível acessar os dados da consulta para a listagem", sqle);
        }

        return viagens;
    }

    @Override
    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException {

        PreparedStatement stmt;
        String sqlProximasViagens = "select * from viagem natural join viagem_fretada where placa=?";

        try {

            stmt = connection.prepareStatement(sqlProximasViagens);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para próximas viagens", sqle);
        }

        try {

            stmt.setString(1, placaVeiculo);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para consultar próximas viagens", sqle);
        }

        return performNextTripsQuery(stmt);
    }

    @Override
    public Collection<Viagem> proximasViagensMotorista(String cpfMotorista) throws PersistenciaException {

        PreparedStatement stmt;
        String sqlProximasViagens = "select * from viagem natural join viagem_fretada where motorista=?";

        try {

            stmt = connection.prepareStatement(sqlProximasViagens);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para próximas viagens", sqle);
        }

        try {

            stmt.setString(1, cpfMotorista);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para consultar próximas viagens", sqle);
        }

        return performNextTripsQuery(stmt);
    }

    private Collection<Viagem> performNextTripsQuery(PreparedStatement stmt) throws PersistenciaException {

        ResultSet result;
        Collection<Viagem> viagens = new ArrayList<>();

        try {

            result = stmt.executeQuery();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível consultar as próximas viagens", sqle);
        }

        try {

            while (result.next()) {

                Veiculo v = new Veiculo(result.getString("placa"));
                Motorista m = new Motorista();
                m.setCpf(result.getString("cpf"));
                ViagemFretada viagem = new ViagemFretada(v, m, Campus.fromString(result.getString("origem")), new Date(result.getTimestamp("data_hora_saida").getTime()), result.getInt("distancia"));
                viagem.setDataHoraRetorno(new Date(result.getTimestamp("data_hora_retorno").getTime()));
                viagens.add(viagem);
            }
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível acessar os dados da consulta para próximas viagens", sqle);
        }

        return viagens;
    }
}
