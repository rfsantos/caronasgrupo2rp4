/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Aplicacao.PersistenciaFactoryPostgres;
import Dominio.Campus;
import Dominio.CategoriaCNH;
import Dominio.Motorista;
import Dominio.Veiculo;
import Dominio.ViagemFretada;
import Excecoes.DadoIncorretoException;
import Excecoes.PersistenciaException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rafael
 */
public class RepositorioViagemFretadaTest {

    RepositorioViagemFretada repVF;
    Veiculo veiculo;
    Motorista motorista;
    ViagemFretada viagem;
    ViagemFretada viagemAlteracao;
    Motorista motoristaAlteracao;
    Veiculo veiculoAlteracao;

    public RepositorioViagemFretadaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        repVF = new PersistenciaFactoryPostgres().createPersistenciaViagemFretada();

        veiculo = new Veiculo("BBB-2222");
        veiculo.setCapacidade(4);
        veiculo.setCategoria_cnh(CategoriaCNH.B);
        veiculo.setCampuOrig(Campus.Alegrete);

        motorista = new Motorista();
        motorista.setCategoriaCNH(CategoriaCNH.B);
        motorista.setCpf("11111111111");
        motorista.setCampus(Campus.Alegrete);

        viagem = new ViagemFretada(veiculo, motorista, Campus.Alegrete, new Date(), 150);
        viagem.setResponsavel("rafael fernando dos santos");
        viagem.setNumPassageiros(3);
        viagem.setCPFResponsavel("11111111111");
        viagem.setDestino("uruguaiana");
        viagem.setDuracao(2.5f);
        viagem.setMotivo("viagem");
        viagem.setDataHoraSaida(new Date(113, 3, 5));
        Calendar c = Calendar.getInstance();
        c.setTime(viagem.getDataHoraSaida());
        c.roll(Calendar.DATE, 2);
        viagem.setDataHoraRetorno(c.getTime());

        veiculoAlteracao = new Veiculo("CCC-3333");
        veiculoAlteracao.setCapacidade(5);
        veiculoAlteracao.setCategoria_cnh(CategoriaCNH.B);
        veiculoAlteracao.setCampuOrig(Campus.Uruguaiana);

        motoristaAlteracao = new Motorista();
        motoristaAlteracao.setCategoriaCNH(CategoriaCNH.B);
        motoristaAlteracao.setCpf("44444444444");
        motoristaAlteracao.setCampus(Campus.Uruguaiana);

        viagemAlteracao = new ViagemFretada(veiculoAlteracao, motoristaAlteracao, Campus.Uruguaiana, new Date(), 600);
        viagemAlteracao.setResponsavel("Joaquim Emanuel Pereira");
        viagemAlteracao.setNumPassageiros(4);
        viagemAlteracao.setCPFResponsavel("38562909823");
        viagemAlteracao.setDestino("Porto Alegre ");
        viagemAlteracao.setDuracao(7);
        viagemAlteracao.setMotivo("Feira");
        //cal.roll(Calendar.DATE, 3);
        //cal.roll(Calendar.SECOND, 1);
        viagemAlteracao.setDataHoraSaida(new Date(113, 3, 12));
        Calendar cal = Calendar.getInstance();
        cal.setTime(viagemAlteracao.getDataHoraSaida());
        cal.roll(Calendar.DATE, 6);
        viagemAlteracao.setDataHoraRetorno(cal.getTime());
    }

    @After
    public void tearDown() {

        repVF = null;
    }

    /**
     * Teste onde todos os dados foram informados corretamente e a inclusão
     * ocorre naturalmente. Para não afetar nos outros testes, recomenda-se
     * desconsiderá-lo após o primeiro teste.
     */
    @Test
    public void testIncluirSucesso() throws Exception {

        System.out.println("Testando inclusão de sucesso");

        this.repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * veículo.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirVeiculoAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de veículo");

        viagem.setVeiculo(null);

        repVF.incluir(viagem);

    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * motorista.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirMotoristaAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de motorista");

        viagem.setMotorista(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: campus
     * de origem.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirCampusOrigemAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de campus de origem");

        viagem.setOrigem(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: cidade
     * de destino.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDestinoAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de cidade de destino");

        viagem.setDestino(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * data/hora de saída.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDateHoraSaidaAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de data/hora de saída");

        viagem.setDataHoraSaida(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * data/hora de retorno.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDataHoraRetornoAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de data/hora de retorno");

        viagem.setDataHoraRetorno(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: nome do
     * responsável.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirResponsavelAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de nome do responsável");

        viagem.setResponsavel(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: CPF do
     * responsável.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirCPFResponsavelAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de CPF do responsável");

        viagem.setCPFResponsavel(null);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * passageiros.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirPassageirosAusente() throws Exception {

        System.out.println("Testando inclusão com ausência de passageiros");

        viagem.setNumPassageiros(0);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * distância entre origem e destino.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDistanciaAusente() throws Exception {

        System.out.println("Testando inclusão quando a distância entre a origem e o destino está ausente");

        viagem.setDistancia(0);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: duração
     * da viagem.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDuracaoAusente() throws Exception {

        System.out.println("Testando inclusão quando a duração da viagem está ausente");

        viagem.setDuracao(0);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a inclusão de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: viagem
     * fretada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirViagemFretadaAusente() throws Exception {

        System.out.println("Testando inclusão com ausência da viagem fretada");

        repVF.incluir(null);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a reserva
     * (inclusão) da viagem fretada não respeita o prazo mínimo de 1 dia de
     * antecedência. Para o teste ser bem sucedido uma exceção do tipo
     * DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirAntecedenciaReserva() throws Exception {

        System.out.println("Testando inclusão para quando o prazo mínimo para reserva é violado");

        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.HOUR, 12);

        viagem.setDataHoraSaida(cal.getTime());

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o veículo
     * requisitado para a viagem não consta no sistema. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirVeiculoInexistente() throws Exception {

        System.out.println("Testando inclusão quando o veículo não consta no sistema");

        Veiculo veic = new Veiculo("KWY-4689");
        veic.setCampuOrig(Campus.Alegrete);
        veic.setCapacidade(4);
        veic.setCategoria_cnh(CategoriaCNH.B);

        viagem.setVeiculo(veic);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o motorista
     * requisitado para a viagem não consta no sistema. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirMotoristaInexistente() throws Exception {

        System.out.println("Testando inclusão quando o motorista não consta no sistema");

        Motorista mot = new Motorista();
        mot.setCpf("01234567891");
        mot.setCategoriaCNH(CategoriaCNH.C);
        mot.setCampus(Campus.Alegrete);

        viagem.setMotorista(mot);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a data/hora de
     * retorno informada é anterior a data/hora de chegada ao destino. Para o
     * teste ser bem sucedido uma exceção do tipo DadoIncorretoException deve
     * ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDataHoraRetornoInvalida() throws Exception {

        System.out.println("Testando inclusão quando a data/hora de retorno é anterior a data/hora de chegada ao destino");

        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.MINUTE, -30);

        viagem.setDataHoraRetorno(cal.getTime());

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o número de
     * passageiros informado está além da capacidade do veículo. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirAlemCapacidadeVeiculo() throws Exception {

        System.out.println("Testando inclusão quando o número de passageiros é maior do que a capacidade do veículo");

        viagem.setNumPassageiros(6);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o número de
     * passageiros informado para a viagem é menor que o limite mínimo exigido.
     * Para o teste ser bem sucedido uma exceção do tipo DadoIncorretoException
     * deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirLimiteMinPassageiros() throws Exception {

        System.out.println("Testando inclusão quando o número de passageiros é inferior ao limite mínimo de passageiros para uma viagem fretada (1)");

        viagem.setNumPassageiros(-1);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o veículo informado
     * não pertence ao campus de onde a viagem partirá. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirVeiculoOutroCampus() throws Exception {

        System.out.println("Testando inclusão quando o veículo não pertence ao campus de onde a viagem partirá");

        viagem.getVeiculo().setCampuOrig(Campus.Jaguarão);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o motorista
     * requisitado não pertence ao campus de onde a viagem partirá. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirMotoristaOutroCampus() throws Exception {

        System.out.println("Testando inclusão quando o motorista não pertence ao campus de onde a viagem partirá");

        viagem.getMotorista().setCampus(Campus.Itaqui);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a categoria de CNH
     * do motorista é incompatível com a categoria exigida pelo veículo. Para o
     * teste ser bem sucedido uma exceção do tipo DadoIncorretoException deve
     * ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirCNHIncompativel() throws Exception {

        System.out.println("Testando inclusão quando a categoria de CNH do motorista é incompatível com a categoria exigida pelo veículo");

        viagem.getMotorista().setCategoriaCNH(CategoriaCNH.A);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a distância entre a
     * origem e o destino não é válida (menor que 0). Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testIncluirDistanciaInvalida() throws Exception {

        System.out.println("Testando inclusão para quando a distância entre a origem o destino é menor que 0");

        viagem.setDistancia(-1);

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o CPF do
     * responsável pela viagem é inválido, isto é, fora do padrão. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testeIncluirCPFInvalido() throws Exception {

        System.out.println("Testando inclusão quando o CPF do responsável é inválido");

        viagem.setCPFResponsavel("135678543");

        repVF.incluir(viagem);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o CPF do
     * responsável pela viagem é inválido, isto é, fora do padrão. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testeIncluirCPFInvalido2() throws Exception {

        System.out.println("Testando inclusão quando o CPF do responsável é inválido. Teste 2");

        viagem.setCPFResponsavel("13456321346753");

        repVF.incluir(viagem);
    }

    /**
     * Teste onde todos os dados foram informados corretamente e a alteração
     * ocorre naturalmente. Para que o teste tenha sucesso primeiro deve ser
     * executado o teste de inclusão. Para não afetar nos outros testes,
     * recomenda-se desconsiderá-lo após o primeiro teste.
     */
    @Test
    public void testAlterarSucesso() throws Exception {

        System.out.println("Testando alteração de sucesso");

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * veículo.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarVeiculoAusente() throws Exception {

        System.out.println("Testando alteração com ausência de veículo");

        viagemAlteracao.setVeiculo(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);

    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * motorista.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarMotoristaAusente() throws Exception {

        System.out.println("Testando alteração com ausência de motorista");

        viagemAlteracao.setMotorista(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: campus
     * de origem.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarCampusOrigemAusente() throws Exception {

        System.out.println("Testando alteração com ausência de campus de origem");

        viagemAlteracao.setOrigem(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: cidade
     * de destino.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDestinoAusente() throws Exception {

        System.out.println("Testando alteração com ausência de cidade de destino");

        viagemAlteracao.setDestino(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * data/hora de saída.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDateHoraSaidaAusente() throws Exception {

        System.out.println("Testando alteração com ausência de data/hora de saída");

        viagemAlteracao.setDataHoraSaida(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * data/hora de retorno.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDataHoraRetornoAusente() throws Exception {

        System.out.println("Testando alteração com ausência de data/hora de retorno");

        viagemAlteracao.setDataHoraRetorno(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: nome do
     * responsável.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarResponsavelAusente() throws Exception {

        System.out.println("Testando alteração com ausência de nome do responsável");

        viagemAlteracao.setResponsavel(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: CPF do
     * responsável.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarCPFResponsavelAusente() throws Exception {

        System.out.println("Testando alteração com ausência de CPF do responsável");

        viagemAlteracao.setCPFResponsavel(null);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * passageiros.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarPassageirosAusente() throws Exception {

        System.out.println("Testando alteração com ausência de passageiros");

        viagemAlteracao.setNumPassageiros(0);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é:
     * distância entre origem e destino.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDistanciaAusente() throws Exception {

        System.out.println("Testando alteração quando a distância entre a origem e o destino está ausente");

        viagemAlteracao.setDistancia(0);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: duração
     * da viagem.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDuracaoAusente() throws Exception {

        System.out.println("Testando alteração quando a duração da viagem está ausente");

        viagemAlteracao.setDuracao(0);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa a ausência de dados
     * necessários para a alteração de uma viagem fretada. Neste caso o sistema
     * deve lançar uma exceção do tipo DadoIncorretoException se o dado
     * necessário não for informado. Portanto, para este teste ter sucesso a
     * exceção esperada deve ser lançada. Os dado ausente nesse teste é: viagem
     * fretada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarViagemFretadaAusente() throws Exception {

        System.out.println("Testando alteração com ausência da viagem fretada");

        repVF.incluir(null);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a reserva
     * (inclusão/alteração) da viagem fretada não respeita o prazo mínimo de 1
     * dia de antecedência. Para o teste ser bem sucedido uma exceção do tipo
     * DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarAntecedenciaReserva() throws Exception {

        System.out.println("Testando alteração para quando o prazo mínimo para reserva é violado");

        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.HOUR, 12);

        viagemAlteracao.setDataHoraSaida(cal.getTime());

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }


    /**
     * Teste para verificar se o sistema detecta e acusa que o veículo
     * requisitado para a viagem não consta no sistema. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarVeiculoInexistente() throws Exception {

        System.out.println("Testando alteração quando o veículo não consta no sistema");

        Veiculo veic = new Veiculo("KWY-4689");
        veic.setCampuOrig(Campus.Alegrete);
        veic.setCapacidade(4);
        veic.setCategoria_cnh(CategoriaCNH.B);

        viagemAlteracao.setVeiculo(veic);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o motorista
     * requisitado para a viagem não consta no sistema. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarMotoristaInexistente() throws Exception {

        System.out.println("Testando alteração quando o motorista não consta no sistema");

        Motorista mot = new Motorista();
        mot.setCpf("01234567891");
        mot.setCategoriaCNH(CategoriaCNH.C);
        mot.setCampus(Campus.Alegrete);

        viagemAlteracao.setMotorista(mot);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a data/hora de
     * retorno informada é anterior a data/hora de chegada ao destino. Para o
     * teste ser bem sucedido uma exceção do tipo DadoIncorretoException deve
     * ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDataHoraRetornoInvalida() throws Exception {

        System.out.println("Testando alteração quando a data/hora de retorno é anterior a data/hora de chegada ao destino");

        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.MINUTE, -30);

        viagemAlteracao.setDataHoraRetorno(cal.getTime());

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o número de
     * passageiros informado está além da capacidade do veículo. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarAlemCapacidadeVeiculo() throws Exception {

        System.out.println("Testando alteração quando o número de passageiros é maior do que a capacidade do veículo");

        viagemAlteracao.setNumPassageiros(6);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o número de
     * passageiros informado para a viagem é menor que o limite mínimo exigido.
     * Para o teste ser bem sucedido uma exceção do tipo DadoIncorretoException
     * deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarLimiteMinPassageiros() throws Exception {

        System.out.println("Testando alteração quando o número de passageiros é inferior ao limite mínimo de passageiros para uma viagem fretada (1)");

        viagemAlteracao.setNumPassageiros(-1);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o veículo informado
     * não pertence ao campus de onde a viagem partirá. Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarVeiculoOutroCampus() throws Exception {

        System.out.println("Testando alteração quando o veículo não pertence ao campus de onde a viagem partirá");

        viagemAlteracao.getVeiculo().setCampuOrig(Campus.Jaguarão);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o motorista
     * requisitado não pertence ao campus de onde a viagem partirá. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarMotoristaOutroCampus() throws Exception {
        System.out.println("Testando alteração quando o motorista não pertence ao campus de onde a viagem partirá");

        viagemAlteracao.getMotorista().setCampus(Campus.Itaqui);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a categoria de CNH
     * do motorista é incompatível com a categoria exigida pelo veículo. Para o
     * teste ser bem sucedido uma exceção do tipo DadoIncorretoException deve
     * ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarCNHIncompativel() throws Exception {

        System.out.println("Testando alteração quando a categoria de CNH do motorista é incompatível com a categoria exigida pelo veículo");

        viagemAlteracao.getMotorista().setCategoriaCNH(CategoriaCNH.A);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que a distância entre a
     * origem e o destino não é válida (menor que 0). Para o teste ser bem
     * sucedido uma exceção do tipo DadoIncorretoException deve ser lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testAlterarDistanciaInvalida() throws Exception {

        System.out.println("Testando alteração para quando a distância entre a origem o destino é menor que 0");

        viagemAlteracao.setDistancia(-1);

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o CPF do
     * responsável pela viagem é inválido, isto é, fora do padrão. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testeAlterarCPFInvalido() throws Exception {

        System.out.println("Testando alteração quando o CPF do responsável é inválido");

        viagemAlteracao.setCPFResponsavel("135678543");

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }

    /**
     * Teste para verificar se o sistema detecta e acusa que o CPF do
     * responsável pela viagem é inválido, isto é, fora do padrão. Para o teste
     * ser bem sucedido uma exceção do tipo DadoIncorretoException deve ser
     * lançada.
     */
    @Test(expected = DadoIncorretoException.class)
    public void testeAlterarCPFInvalido2() throws Exception {

        System.out.println("Testando alteração quando o CPF do responsável é inválido. Teste 2");

        viagemAlteracao.setCPFResponsavel("13456321346753");

        this.repVF.alterar(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida(), viagemAlteracao);
    }
    
    /**
     * Teste de exclusão de viagem fretada quando todos os dados passados estão corretos e a exlusão pode ser efetuada.
     * Para que o teste seja possível deve-se fazer a inclusão (testIncluirSucesso) e não realizar a alteração (testAlteraçãoSucesso).
     */
    @Test
    public void testExcluirSucesso() throws Exception{
        
        System.out.println("Testando exclusão de viagem de sucesso");
        
        repVF.excluir(viagem.getVeiculo().getPlaca(), viagem.getDataHoraSaida());
    }
}