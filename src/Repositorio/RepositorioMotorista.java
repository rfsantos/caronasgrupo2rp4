/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Dominio.Motorista;
import Dominio.Verificacoes;
import Persistencia.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import persistencia.MotoristaDAO;

/**
 *
 * @author NoteCCE
 */
public class RepositorioMotorista {
    
   private MotoristaDAO mDao;
    
    public RepositorioMotorista(MotoristaDAO mDao){
        
        this.mDao = mDao;
    }
    
    /**
     * Metodo que permite alterar motorista no banco de dados
     * @return 
     */
    public boolean alterarMotorista(Motorista motorista) {
         if (motorista != null){
            try {
                if( !Verificacoes.VerUsuarioExiste(motorista.getUsuario())
                    && Verificacoes.VerEmail(motorista.getEmail())
                    && verCnh(motorista.getCnh())) {
               
                    return mDao.alterarMotorista(motorista);
                }else{
                    System.out.println("RETORNO FALSE -> Nao foi possivel alterar motorista");
                    return false;
                }
            } catch (    SQLException | ClassNotFoundException ex) {
                Logger.getLogger(RepositorioMotorista.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("RETORNO FALSE -> Erro na alteracao de motorista (objeto == null)");
            return false;
        }
        return false;

    }
    
    /**
     * Metodo que permite listar motoristas do banco de dados
     * @param motorista
     * @return 
     */
    public List listarMotoristas(ArrayList motorista){
        return null;
        
    }
    
    /**
     * Metodo que permite desativar motorista no banco de dados
     * @param cpf
     * @return 
     */
    public boolean desativarMotorista(Motorista motorista){
        
   /**      if (motorista != null ){
            if(!verViagens(motorista.getCpf()) {
                return mDao.desativarMotorista(motorista);
            }else{
                System.out.println("RETORNO FALSE -> Nao foi possivel desativar motorista ");
                return false;
            }    
        } else {
            System.out.println("RETORNO FALSE -> Erro na desativacao de motorista (objeto null)");
    return false;
        }
      */
        return false;
      
    }
    
    /**
     * Metodo que permite incluir motorista no bando de dados
     * @param motorista
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public boolean incluirMotorista(Motorista motorista) throws SQLException, ClassNotFoundException{
        
         if (motorista != null ){
            
             /*
            if( !Verificacoes.VerUsuarioExiste(motorista.getUsuario())
                && Verificacoes.VerCpfValido(motorista.getCpf())
                && Verificacoes.VerEmail(motorista.getEmail())
                && verCnh(motorista.getCnh())) {
                
                return mDao.incluirMotorista(motorista);
            }else{
                System.out.println("RETORNO FALSE -> Inclusao do mortorista nao realizada");
                return false;
            }
            */
             return mDao.incluirMotorista(motorista);
        } else {
            System.out.println("RETORNO FALSE -> Erro na inclusao do motorista (objeto null)");
            return false;
        }
    }
       
    /**
     * Metodo que permite buscar motorista no banco de dados
     * @param cpf
     * @return 
     */
    public Motorista buscarMotorista(String cpf){
        
        return mDao.buscaMotorista(cpf);
    }
    
 /**   public boolean verViagens(String cpf){
        
         if (mDao.buscaQtdViagens() > 0) {
            System.out.println("EXISTE PELO MENOS UMA VIAGEM PENDENTE");
            return true;
        } else {
            System.out.println("NAO EXISTE NENHUMA VIAGEM PENDENTE");
            return false;
        }

        return true;
    }
    **/
  
    
    /**
     * Metodo que verifica Cnh valido
     * @param cnh
     * @return 
     */
    public boolean verCnh(String cnh){
        
        Pattern p = Pattern.compile("d{11}");
        Matcher m = p.matcher(cnh);
        boolean matchFound = m.matches();

        if (matchFound) {
            System.out.println("RETORNO TRUE -> CNH valido");
            return true;
        } else {
            System.out.println("RETORNO FALSE -> CNH invalido");
            return false;
        }     
    }
    
       /**
     * Metodo que ferifica se numero de cpf já existe
     * @param cpf
     * @return 
     */
    public static boolean CnhExiste(String cnh) throws SQLException{
        
    Connection con = ConnectionFactory.createConnectionPostgres();      
    String vSQL = "select * from motorista where cnh";       
    PreparedStatement com = (PreparedStatement) con.prepareStatement(vSQL);       
    com.setString(1, cnh);      
    ResultSet rs = com.executeQuery();       
    Motorista m = null;    
    
    if (rs.next()) {    
    //    m = new Motorista();    
            
    //    m.setNome(rs.getString("usuario"));
        return true;
    }    
    return false;       
    }
    
}
