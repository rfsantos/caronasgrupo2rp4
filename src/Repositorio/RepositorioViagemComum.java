/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Dominio.Viagem;
import Excecoes.PersistenciaException;
import java.util.Collection;
import persistencia.ViagemComumDAO;

/**
 *
 * @author rafael
 */
public class RepositorioViagemComum{
    
    ViagemComumDAO vcDAO;
    
    public RepositorioViagemComum(ViagemComumDAO vcDAO){
        
        this.vcDAO = vcDAO;
    }
    
    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException{
        
        return vcDAO.proximasViagensVeiculo(placaVeiculo);
    }
}
