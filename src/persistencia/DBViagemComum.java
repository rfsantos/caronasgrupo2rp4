/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.Motorista;
import Dominio.Veiculo;
import Dominio.Viagem;
import Dominio.ViagemComum;
import Dominio.ViagemFretada;
import Excecoes.PersistenciaException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author rafael
 */
public class DBViagemComum implements ViagemComumDAO {

    Connection connection;

    public DBViagemComum(Connection connection) {

        this.connection = connection;
    }

    @Override
    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException {

        String sqlProximasViagens = "select * from (viagem_comum natural join viagem) as viagem_ida inner join (viagem_comum natural join viagem) as viagem_retorno  on viagem_ida.data_hora_saida_retorno = viagem_retorno.data_hora_saida and viagem_ida.placa_retorno = viagem_retorno.placa where viagem_ida.placa = ?";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlProximasViagens);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para próximas viagens", sqle);
        }

        try {

            stmt.setString(1, placaVeiculo);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para consultar próximas viagens", sqle);
        }

        return performNextTripsQuery(stmt);
    }

    @Override
    public Collection<Viagem> proximasViagensMotorista(String cpfMotorista) throws PersistenciaException {

        String sqlProximasViagens = "select * from (viagem_comum natural join viagem) as viagem_ida inner join (viagem_comum natural join viagem) as viagem_retorno  on viagem_ida.data_hora_saida_retorno = viagem_retorno.data_hora_saida and viagem_ida.placa_retorno = viagem_retorno.placa where viagem_ida.motorista = ?";
        PreparedStatement stmt;

        try {

            stmt = connection.prepareStatement(sqlProximasViagens);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para próximas viagens", sqle);
        }

        try {

            stmt.setString(1, cpfMotorista);
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível configurar o sql para consultar próximas viagens", sqle);
        }

        return performNextTripsQuery(stmt);
    }

    private Collection<Viagem> performNextTripsQuery(PreparedStatement stmt) throws PersistenciaException {

        ResultSet result;
        Collection<Viagem> viagens = new ArrayList<>();

        try {

            result = stmt.executeQuery();
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível consultar as próximas viagens", sqle);
        }

        try {

            while (result.next()) {

                Veiculo v = new Veiculo(result.getString(2));
                Motorista m = new Motorista();
                m.setCpf(result.getString(8));
                ViagemComum vcRetorno = new ViagemComum(v, m, Campus.fromString(result.getString(18)), new Date(result.getTimestamp(10).getTime()), result.getFloat(16));
                ViagemComum viagem = new ViagemComum(v, m, Campus.fromString(result.getString(9)), new Date(result.getTimestamp(1).getTime()), result.getFloat(7), vcRetorno);
                viagens.add(viagem);
            }
        } catch (SQLException sqle) {

            throw new PersistenciaException("Não foi possível acessar os dados da consulta para próximas viagens", sqle);
        }

        return viagens;
    }
}
