/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

public class Caronista {

    private String nome;
    private String CPF;
    private String email;
    
    
    public Caronista(){}
    
    
    public Caronista(String nome,String email){
        
       setEmail(email);
       setNome(nome);
    }
    

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the CPF
     */
    public String getCPF() {
        return CPF;
    }

    /**
     * @param CPF the CPF to set
     */
    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
