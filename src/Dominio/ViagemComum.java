package Dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rafael
 */
public class ViagemComum extends Viagem {

    private ViagemComum viagemRetorno;
    private Campus campusDestino;
    private Material mat;
    private List<Caronista> caronistas;
    
    public ViagemComum(Motorista mot, Date date, Material mat ){
        
        this(null, mot, null, date, 0);
    }
    
    public ViagemComum(Veiculo veiculo, Motorista motorista, Campus origem, Date dataHoraSaida, float distancia) {

        super(veiculo, motorista, origem, dataHoraSaida, distancia);
        this.caronistas = new ArrayList<>();
    }

    public ViagemComum(Veiculo veiculo, Motorista motorista, Campus origem, Date dataHoraSaida, float distancia, ViagemComum viagemRetorno) {

        this(veiculo, motorista, origem, dataHoraSaida, distancia);
        this.viagemRetorno = viagemRetorno;
    }

    @Override
    public Date getDataHoraRetorno() {

        if (viagemRetorno != null) {

            return viagemRetorno.getDataHoraSaida();
        }

        throw new UnsupportedOperationException("Não há horário de retorno para uma viagem de retorno");
    }

    @Override
    public String getDestino() {

        return campusDestino.toString();
    }

    /**
     * @return the caronistas
     */
    public List<Caronista> getCaronistas() {
        return caronistas;
    }

    /**
     * @param caronistas the caronistas to set
     */
    public void addCaronista(Caronista caronista) {
        this.caronistas.add(caronista);
    }
    
    public void removeCaronista(Caronista caronista){
        this.caronistas.remove(caronista);
    }
}
