/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Excecoes;

/**
 *
 * @author rafael
 */
public class PersistenciaException extends Exception {
    
    private Exception ex;
    
    public PersistenciaException(String message, Exception ex){
       
        super(message);
        this.ex = ex;
    }
    
    public Exception getException(){
        
        return ex;
    }
}
