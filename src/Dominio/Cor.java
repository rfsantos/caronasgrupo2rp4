/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

/**
 *
 * @author ricardo
 */
public enum Cor {
    azul,
    amarelo,
    vermelho,
    preto,
    laranja,
    cinza,
    dourado,
    verde,
    branco;

    public static Cor fromString(String text) {
        if (text != null) {
            for (Cor c : Cor.values()) {
                if (text.equalsIgnoreCase(c.toString())) {
                    return c;
                }
            }
        }
        return null;
    }
}
