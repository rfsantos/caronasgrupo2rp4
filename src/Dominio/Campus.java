/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

/**
 *
 * @author rafael
 */
public enum Campus {

    Alegrete,
    São_Borja,
    Uruguaiana,
    São_Gabriel,
    Caçapava_do_Sul,
    Dom_Pedrito,
    Itaqui,
    Jaguarão,
    Santana_do_Livramento,
    Bagé;

    public static Campus fromString(String text) {
        if (text != null) {
            for (Campus c : Campus.values()) {
                if (text.equalsIgnoreCase(c.toString())) {
                    return c;
                }
            }
        }
        return null;
    }
    
    
    
    

}
