﻿/*************CADASTRO DE VEICULOS**************/


INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('AAA-1111','Volkswagen','Gol 2011','2011',4,8900,'Bagé','cinza','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('BBB-2222','Volkswagen','Gol 2012','2012',5,900,'Alegrete','verde','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('CCC-3333','Audi','A1 Sport','2013',5,800,'Uruguaiana','vermelho','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('DDD-4444','Audi','S6 Sedan','2013',5,8200,'Alegrete','branco','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('EEE-5555','Audi','R8 GT','2011',5,1350,'Alegrete','preto','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('FFF-6666','Volkswagen','Parati','2012',5,2310,'Alegrete','branco','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('GGG-7777','Volkswagen','Parati','2010',4,2310,'Alegrete','azul','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('HHH-8888','Peugeot','Peugeot 207','2012',4,6790,'Alegrete','branco','B','true');

INSERT INTO VEICULO (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('III-9999','Peugeot','Peugeot 208','2012',4,1003,'Alegrete','preto','B','true');

INSERT INTO veiculo (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('JJJ-0000','FIAT','Fiat2013', 2013,5,200,'Alegrete','branco','B','true');

INSERT INTO veiculo (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('KKK-1111','FIAT','Fiat2013', 2013,5,200,'Alegrete','branco','B','true');
	
INSERT INTO veiculo (placa,marca,modelo,ano,capacidade,quilometragem,campus,cor,categoria_cnh,ativo)
	VALUES ('YYY-2222','FIAT','Fiat Uno 2013', 2013,5,200,'Alegrete','preto','B','true');



/**************CADASTRO MODO ACESSO***************/

INSERT INTO modo_acesso (cod_modo_acesso, descricao) VALUES (1, 'administrador');
INSERT INTO modo_acesso (cod_modo_acesso, descricao) VALUES (2, 'comum');

/**************CADASTRO LOGIN***************/

INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('maria','maria',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('paulo','paulo',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('joao','joao',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('juca','juca',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('robson','robson',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('joaquim','joaquim',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('ricardo','ricardo',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('mariza','mariza',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('ana','ana',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('juresvaldo','juresvaldo',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('antonio','antonio',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('pablo','pablo',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('marcia','marcia',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('claudia','claudia',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('rafael','rafael',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('rogerio','rogerio',1);
INSERT INTO login (usuario,senha,cod_modo_acesso) VALUES ('ivone','ivone',1);

/**************CADASTRO MOTORISTA***************/

INSERT INTO motorista (cpf,nome,email,cnh,data_nascimento,data_contrat,campus,usuario, categoria_cnh, ativo) 
	VALUES ('11111111111','Maria dos Santos','maria@gmail.com','22222222222','01.01.1980','25.01.2009', 'Alegrete','maria', 'B', 'true');

INSERT INTO motorista (cpf,nome,email,cnh,data_nascimento,data_contrat,campus,usuario, categoria_cnh, ativo) 
	VALUES ('22222222222','Paulo Barbosa','paulo@gmail.com','33333333333','01.01.1980','25.01.2009', 'Alegrete','paulo', 'B', 'true');

INSERT INTO motorista (cpf,nome,email,cnh,data_nascimento,data_contrat,campus,usuario, categoria_cnh, ativo) 
	VALUES ('33333333333','João','joao@gmail.com','44444444444','01.01.1980','25.01.2009', 'Alegrete','joao', 'B', 'true');

INSERT INTO motorista (cpf,nome,email,cnh,data_nascimento,data_contrat,campus,usuario, categoria_cnh, ativo) 
	VALUES ('44444444444','Juca','juca@gmail.com','55555555555','01.01.1980','25.01.2009', 'Uruguaiana','juca', 'B', 'true');

/**************CADASTRO CARONISTA***************/

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('11111AAAA', '55555555555', 'Robson', 'robson@gmail.com','22/01/1970','robson');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('22222BBBB', '66666666666', 'Joaquim', 'joaquim@gmail.com','19/01/1975','joaquim');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('33333CCCC', '77777777777', 'Ricardo', 'ricardo@gmail.com','19/01/1975','ricardo');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('44444DDDD', '88888888888', 'Mariza', 'mariza@gmail.com','19/01/1975','mariza');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('55555EEEE', '99999999999', 'Ana', 'ana@gmail.com','19/01/1975','ana');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('66666FFFF', '99999000000', 'juresvaldo', 'juresvaldo@gmail.com','19/01/1975','juresvaldo');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('77777GGGG', '99999111111', 'Antonio', 'antonio@gmail.com','19/01/1975','antonio');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('88888HHHH', '99999222222', 'Pablo', 'pablo@gmail.com','19/01/1975','pablo');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('99999IIII', '99999333333', 'Marcia', 'marcia@gmail.com','19/01/1975','marcia');
	
INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('99999JJJJ', '99999444444', 'Claudia', 'claudia@gmail.com','19/01/1975','claudia');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('99999KKKK', '99999555555', 'Rafael', 'rafael@gmail.com','19/01/1975','rafael');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('99999LLLL', '99999666666', 'Rogério', 'rogerio@gmail.com','19/01/1975','rogerio');

INSERT INTO caronista (matricula, cpf, nome, email, data_nasc, usuario)
	VALUES ('99999MMMM', '99999777777', 'Ivone', 'ivone@gmail.com','19/01/1975','ivone');

/**************CADASTRO VIAGENS COMUM***************/

/*dependendo da versão de criação do BD não precisa deste comando abaixo
ALTER TABLE viagem_comum DROP constraint viagem_comum_viagem_comum_fk 
*/


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('30.03.2013 13:00:00', 'AAA-1111', 120, 11111111111, 'Uruguaiana');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino) 
		  VALUES ('30.03.2013 13:00:00', 'AAA-1111', 'tonner recarga impressora', 'Alegrete'); 

INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('29.03.2013 08:00:00', 'AAA-1111', 120,  11111111111, 'Alegrete');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino, data_hora_saida_retorno, placa_retorno) 
		  VALUES ('29.03.2013 08:00:00', 'AAA-1111', 'documentos', 'Uruguaiana', '30.03.2013 13:00:00', 'AAA-1111'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('05.04.2013 22:15:00', 'KKK-1111', 120, 33333333333, 'São_Gabriel');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino) 
		  VALUES ('05.04.2013 22:15:00', 'KKK-1111', 'material laboratório', 'Alegrete'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('30.03.2013 10:35:00', 'KKK-1111', 120, 33333333333, 'Alegrete');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino, data_hora_saida_retorno, placa_retorno) 
		  VALUES ('30.03.2013 10:35:00', 'KKK-1111', 'notebook', 'São_Gabriel', '05.04.2013 22:15:00', 'KKK-1111'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('17.05.2013 19:30:00', 'CCC-3333', 200, 44444444444, 'Santana_do_Livramento');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino) 
		  VALUES ('17.05.2013 19:30:00', 'CCC-3333', 'Folhas A-4', 'Uruguaiana'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('06.05.2013 19:40:00', 'CCC-3333', 200, 44444444444, 'Uruguaiana');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino, data_hora_saida_retorno, placa_retorno) 
		  VALUES ('06.05.2013 19:40:00', 'CCC-3333', '', 'Santana_do_Livramento', '17.05.2013 19:30:00', 'CCC-3333'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('17.05.2013 21:40:00', 'BBB-2222', 350, 22222222222, 'Jaguarão');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino) 
		  VALUES ('17.05.2013 21:40:00', 'BBB-2222', '', 'Alegrete'); 


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('17.05.2013 08:05:00', 'BBB-2222', 350, 22222222222, 'Alegrete');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino, data_hora_saida_retorno, placa_retorno) 
		  VALUES ('17.05.2013 08:05:00', 'BBB-2222', '', 'Jaguarão', '17.05.2013 21:40:00', 'BBB-2222');


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('21.05.2013 06:15:00', 'BBB-2222', 200, 11111111111, 'Itaqui');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino) 
		  VALUES ('21.05.2013 06:15:00', 'BBB-2222', '', 'Alegrete');


INSERT INTO viagem (data_hora_saida, placa, distancia, cpf, origem) 
	VALUES ('18.05.2013 06:15:00', 'BBB-2222', 200, 11111111111, 'Alegrete');
INSERT INTO viagem_comum (data_hora_saida, placa, material, destino, data_hora_saida_retorno, placa_retorno) 
		  VALUES ('18.05.2013 06:15:00', 'BBB-2222', '', 'Itaqui', '21.05.2013 06:15:00', 'BBB-2222');



/**************CADASTRO CARONAS***************/

INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('29.03.2013 08:00:00', 'AAA-1111', '11111AAAA');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('29.03.2013 08:00:00', 'AAA-1111', '88888HHHH');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('29.03.2013 08:00:00', 'AAA-1111', '99999JJJJ');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('29.03.2013 08:00:00', 'AAA-1111', '99999LLLL');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('30.03.2013 10:35:00', 'KKK-1111', '66666FFFF');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('30.03.2013 10:35:00', 'KKK-1111', '33333CCCC');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 19:30:00', 'CCC-3333', '99999MMMM');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 19:30:00', 'CCC-3333', '22222BBBB');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('06.05.2013 19:40:00', 'CCC-3333', '33333CCCC');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('06.05.2013 19:40:00', 'CCC-3333', '44444DDDD');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 08:05:00', 'BBB-2222', '55555EEEE');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 08:05:00', 'BBB-2222', '77777GGGG');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 08:05:00', 'BBB-2222', '99999JJJJ');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 21:40:00', 'BBB-2222', '11111AAAA');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('17.05.2013 21:40:00', 'BBB-2222', '55555EEEE');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('18.05.2013 06:15:00', 'BBB-2222', '22222BBBB');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('21.05.2013 06:15:00', 'BBB-2222', '88888HHHH');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('21.05.2013 06:15:00', 'BBB-2222', '33333CCCC');
INSERT INTO carona (data_hora_saida, placa, matricula) VALUES ('21.05.2013 06:15:00', 'BBB-2222', '99999LLLL');







