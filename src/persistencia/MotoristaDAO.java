/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.Motorista;
import java.util.List;

/**
 *
 * @author NoteCCE
 */
public interface MotoristaDAO {
    
    public boolean incluirMotorista(Motorista m);
    
    public boolean desativarMotorista (String cpf);
    
    public List listarMotoristas(Campus c);
    
    public boolean alterarMotorista(Motorista motorista);
    
    public Motorista buscaMotorista(String cpf);
}
