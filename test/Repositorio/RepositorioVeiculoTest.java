/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositorio;

import Aplicacao.PersistenciaFactoryPostgres;
import Dominio.Campus;
import Dominio.CategoriaCNH;
import Dominio.Cor;
import Dominio.Veiculo;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Burg Machado
 */
public class RepositorioVeiculoTest {
    
    public RepositorioVeiculoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    

    // ************************* TESTES RELACIONADOS A INCLUSÃO DE VEICULO ************************* */
    //********************************************************************************************** */
    
    /**
     * Teste que tem por objetivo de testar o método incluir veiculo, sendo passado como 
     * parâmetro um objeto nulo, o método deve ser capaz de detectar se o objeto é nulo e encerrar a operação
     * @return false
     */
    @Test
    public void testeIncluirVeiculoObjetoNull_1() {
        System.out.println(" ************ Teste incluir veiculo -> objeto null");
        Veiculo veiculo = null;
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(veiculo);
        assertEquals(expResult, result);
     }
     
     /**
     * Teste que tem por objetivo testar o método incluir veiculo, sendo passado como 
     * parâmetro um objeto NÃO nulo, o método deve ser capaz de detectar se o objeto NÃO é nulo e executar toda a operação de inclusão com sucesso
     * @return true
     */
    @Test
    public void testeIncluirVeiculoObjetoNull_2() {
        System.out.println(" ************ Teste incluir veiculo -> objeto não null");
        Veiculo veiculo = new Veiculo("AAA-0001", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B") );
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0001";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        //executa a inclusão do veiculo no banco de dados
        boolean result = instance.incluir(veiculo);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0001");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());  
     }
     
     /**
     * Teste que tem por objetivo testar o método incluir veiculo, passando como parâmetro
     * uma placa já existente no banco de dados
     * Obs.: Ordem dos parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVeiculoVerifPlacaExist_1(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa existente");
        Veiculo v = 
        new Veiculo("AAA-2222", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B") );
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Teste que tem por objetivo testar o método incluir veiculo, passando como parâmetro
     * uma placa NÃO existente no banco de dados
     * Obs.: Ordem dos parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */

    public void testeIncluirVeiculoVerifPlacaExist_2(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa existente");
        Veiculo v = 
        new Veiculo("AAA-0002", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B") );
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0002";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.incluir(v);
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0002");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());  
    }
    
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_1(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("Aaa-3333", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
     /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_2(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AB2-3333", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_3(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AAA-B333", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_4(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AAA-12345", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_5(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AAA1234", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
     /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que não esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifPlacaValida_6(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AAA 1234", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma placa que esteja dentro dos padrões da seguinte expressão regular "[A-Z]{3}-\\d{4}"), 
     * cuja mesma significa: três letras maiusculas seguidas de um ífen e seguido de 4 números/dígitos inteiros
     * Obs.: Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeIncluirVerifPlacaValida_7(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("AAA-0003", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
       
        //Resultados esperados:
        String expResultPlaca = "AAA-0003";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
         boolean result = instance.incluir(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0003");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro um ano que não esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void teesteIncluirVerifAnoValido_1(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica ano válido");
        Veiculo v = 
        new Veiculo("ABC-1111", "Volkswagen", "Gol 2011", 12345, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro um ano que não esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifAnoValido_2(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica ano válido");
        Veiculo v = 
        new Veiculo("ABC-1111", "Volkswagen", "Gol 2011", 1949, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro um ano que não esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifAnoValido_3(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica ano válido");
        Veiculo v = 
        new Veiculo("ABC-1111", "Volkswagen", "Gol 2011", -1951, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro um ano que não esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifAnoValido_4(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica ano válido");
        Veiculo v = 
        new Veiculo("ABC-1111", "Volkswagen", "Gol 2011", Integer.parseInt("19500"), 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro um ano que esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeIncluirVerifAnoValido_5(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica ano válido");
        Veiculo v = 
        new Veiculo("AAA-0004", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0004";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.incluir(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0004");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());
    }
    
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifCapacValida_1(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-8888", "Volkswagen", "Gol 2011", 2011, -1, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma capacidade que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifCapacValida_2(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, 1, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método incluir veiculo, 
     * passando como parâmetro uma capacidade que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifCapacValida_3(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, -1, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma capacidade que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifCapacValida_4(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, 00001, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma capacidade que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeIncluirVerifCapacValida_5(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, 2, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeIncluirVerifCapacValida_6(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0005";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.incluir(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0005");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeIncluirVerifCapacValida_7(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica capacidade válida");
        Veiculo v = 
        new Veiculo("AAA-0005", "Volkswagen", "Gol 2011", 2011, 100, 
                Cor.azul, Campus.Bagé, 8900, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0005";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 100;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 8900;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.incluir(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0005");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void terstIncluirVerifQuilomet_1(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica quilometragem");
        Veiculo v = 
                
        new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 0, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void terstIncluirVerifQuilomet_2(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica quilometragem");
        Veiculo v = 
                
        new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, -1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.incluir(v);
        assertEquals(expResult, result);
    }

    
    /**
     * Este teste tem por objetivo testar o método "incluir veiculo", 
     * passando como parâmetro uma quilometragem que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Ordem parâmetros Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void terstIncluirVerifQuilomet_3(){
        
        System.out.println(" ************ Teste incluir veiculo -> verifica quilometragem");
        Veiculo v = new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", 2011, 4, 
                Cor.azul, Campus.Bagé, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-0006";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 2011;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.azul;
        Campus expResultCampus = Campus.Bagé;
        int expResultQuilometragem = 1;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.incluir(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-0006");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    
    // ************************* TESTES RELACIONADOS A DESATIVAÇÃO DE VEICULO ************************* */
    //************************************************************************************************* */
    /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro uma placa de um veiculo cadastrado no banco de dados que tenha  no mímimo uma viagem agendada com data igual ou superior a data atual
     * @return false
     */
    
    @Test
    public void testeDesativarVeiculoVerifViagPendente_1(){
        
        System.out.println(" ************ Teste desativar veiculo -> verifica viagem pendente");
        Veiculo v = new Veiculo("AAA-1111");
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.desativar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro uma placa de um veiculo cadastrado no banco de dados que tenha  no mímimo uma viagem agendada com data igual ou superior a data atual
     * @return false
     */
    
    @Test
    public void testeDesativarVeiculoVerifViagPendente_2(){
        
        System.out.println(" ************ Teste desativar veiculo -> verifica viagem pendente");
        Veiculo v = new Veiculo("HHH-8888");
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.desativar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro uma placa de um veiculo cadastrado no banco de dados que NÃO tenha nehuma viagem agendada com data igual ou superior a data atual
     * @return true
     */
    @Test
    public void testeDesativarVeiculoVerifViagPendente_3(){
        
        System.out.println(" ************ Teste desativar veiculo -> verifica viagem pendente");
        Veiculo v = new Veiculo("AAA-8888");
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        
        //Resultados esperados:
        String expResultPlaca = "AAA-8888";
        boolean expResultAtivo = false;
        
        boolean result = instance.desativar(v);
        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-8888");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultAtivo, veiBD.getAtivo());
    }
    
    
    /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro um objeto do tipo veiculo nulo
     * @return false
     */
    @Test
    public void testeDesativarVeiculoObjetoNull(){
        
        System.out.println(" ************ Teste desativar veiculo -> objeto nulo");
        Veiculo v = null;
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.desativar(v);
        assertEquals(expResult, result);
    }
    
   
    /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro uma placa inválida
     * @return false
     */
    @Test
    public void testeDesativarVeiculoVerificaPlacaValida_1(){
        
        System.out.println(" ************ Teste desativar veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("aAA-12345");
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.desativar(v);
        assertEquals(expResult, result);
    }
    
     /**
     * Este teste tem por objetivo testar o método "desativação de veiculo", 
     * passando como parâmetro uma placa inválida
     * @return false
     */
    @Test
    public void testeDesativarVeiculoVerificaPlacaValida_2(){
        
        System.out.println(" ************ Teste desativar veiculo -> verifica placa válida");
        Veiculo v = 
        new Veiculo("BBB 1234");
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.desativar(v);
        assertEquals(expResult, result);
    }

    // ************************* TESTES RELACIONADOS A ALTERAÇÃO DE DADOS DE UM VEICULO ************************* */
    //*********************************************************************************************************** */
     
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, sendo passado como parametro um objeto null
     * @return false
     */
    @Test
    public void testeAlterarVeiculoObjetoNull(){
        
        System.out.println(" ************ Teste alterar veiculo -> objeto null");
        Veiculo v = null;
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro um ano que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifAnoValido_1(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica ano válido");
        Veiculo v = new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", 1949, 4, 
                Cor.azul, Campus.Bagé, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro um ano que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifAnoValido_2(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica ano válido");
        Veiculo v = new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", -1000, 4, 
                Cor.azul, Campus.Bagé, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro um ano que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifAnoValido_3(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica ano válido");
        Veiculo v = new Veiculo("AAA-0006", "Volkswagen", "Gol 2011", 000, 4, 
                Cor.azul, Campus.Bagé, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro um ano que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{4}"), 
     * cuja mesma significa: 4 números/dígitos inteiros, além disso, é verificado se o valor é igual ou maior do que 1950
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeAlterarVeiculoVerifAnoValido_4(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica ano válido");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, 4, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        
        //Resultados esperados:
        String expResultPlaca = "AAA-1111";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 1950;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.amarelo;
        Campus expResultCampus = Campus.Alegrete;
        int expResultQuilometragem = 1;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.alterar(v);        
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-1111");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifCapacidValida_1(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica capacidade válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, -5, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifCapacidValida_2(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica capacidade válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, 2, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifCapacidValida_3(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica capacidade válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, 0, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma capacidade que esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 2
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeAlterarVeiculoVerifCapacidValida_4(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica capacidade válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1952, 4, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();

        //Resultados esperados:
        String expResultPlaca = "AAA-1111";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 1952;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.amarelo;
        Campus expResultCampus = Campus.Alegrete;
        int expResultQuilometragem = 1;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.alterar(v);
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-1111");
         
        //verifica os resultados esperados
        assertEquals(result, true);        
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifQuilometValida_1(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica quilometragem válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, 4, 
                Cor.amarelo, Campus.Alegrete, 0, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }
    
    
     /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return false
     */
    @Test
    public void testeAlterarVeiculoVerifQuilometValida_2(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica quilometragem válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1950, 4, 
                Cor.amarelo, Campus.Alegrete, -5, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();
        boolean expResult = false;
        boolean result = instance.alterar(v);
        assertEquals(expResult, result);
    }

     /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test   
    public void testeAlterarVeiculoVerifQuilometValida_3(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica quilometragem válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1954, 4, 
                Cor.amarelo, Campus.Alegrete, 1, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();

        //Resultados esperados:
        String expResultPlaca = "AAA-1111";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 1954;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.amarelo;
        Campus expResultCampus = Campus.Alegrete;
        int expResultQuilometragem = 1;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.alterar(v);
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-1111");
         
        //verifica os resultados esperados
        assertEquals(result, true);
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    /**
     * Este teste tem por objetivo testar a alteração de um veiculo, 
     * passando como parâmetro uma quilometragem que NÃO esteja dentro dos padrões da seguinte expressão regular "\\d{1,}"), 
     * cuja mesma significa: tem que ter no mínimo um número inteiro e somente números, além disso, é verificado se o valor é maior do que 0
     * Obs.: Ordem parâmetros: Veiculo('PLACA','MARCA','MODELO',ANO,CAPACIDADE,COR,CAMPUS,QUILOMETRAGEM,CAT.CNH)
     * @return true
     */
    @Test
    public void testeAlterarVeiculoVerifQuilometValida_4(){
        
        System.out.println(" ************ Teste alterar veiculo -> verifica quilometragem válida");
        Veiculo v = new Veiculo("AAA-1111", "Volkswagen", "Gol 2011", 1955, 4, 
                Cor.amarelo, Campus.Alegrete, 1000000000, CategoriaCNH.fromString("B"));
        RepositorioVeiculo instance = new PersistenciaFactoryPostgres().createPersistenciaVeiculo();

        //Resultados esperados:
        String expResultPlaca = "AAA-1111";
        String expResultMarca = "Volkswagen";
        String expResultModelo = "Gol 2011";
        int expResultAno = 1955;
        int expResultCapacidade = 4;
        Cor expResultCor = Cor.amarelo;
        Campus expResultCampus = Campus.Alegrete;
        int expResultQuilometragem = 1000000000;
        CategoriaCNH expResultCateg_CNH = CategoriaCNH.fromString("B");
        
        boolean result = instance.alterar(v);
        //recupera o veiculo recém cadastrado no banco de dados
        Veiculo veiBD = instance.buscaVeiculo("AAA-1111");
         
        //verifica os resultados esperados
        assertEquals(result, true);
        assertEquals(expResultPlaca, veiBD.getPlaca());
        assertEquals(expResultMarca, veiBD.getMarca());
        assertEquals(expResultModelo, veiBD.getModelo());
        assertEquals(expResultAno, veiBD.getAno());
        assertEquals(expResultCapacidade, veiBD.getCapacidade());
        assertEquals(expResultCor, veiBD.getCor());
        assertEquals(expResultCampus, veiBD.getCampuOrig());
        assertEquals(expResultQuilometragem, veiBD.getQuilometragem());
        assertEquals(expResultCateg_CNH, veiBD.getCategoria_cnh());   
    }
    
    
}
