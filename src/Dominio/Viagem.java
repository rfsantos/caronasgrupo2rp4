/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import java.util.Date;

/**
 *
 * @author rafael
 */
public abstract class Viagem {

    private Veiculo veiculo;
    private Motorista motorista;
    private Campus origem;
    private Date dataHoraSaida;
    private float distancia;

    public Viagem() {   }
    
    
    public Viagem(Veiculo veiculo, Motorista motorista, Campus origem, Date dataHoraSaida, float distancia) {
        
        this.veiculo = veiculo;
        this.motorista = motorista;
        this.origem = origem;
        this.dataHoraSaida = dataHoraSaida;
        this.distancia = distancia;
    }

    /**
     * @return the veiculo
     */
    public Veiculo getVeiculo() {
        return veiculo;
    }

    /**
     * @param veiculo the veiculo to set
     */
    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    /**
     * @return the motorista
     */
    public Motorista getMotorista() {
        return motorista;
    }

    /**
     * @param motorista the motorista to set
     */
    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

    /**
     * @return the origem
     */
    public Campus getOrigem() {
        return origem;
    }

    /**
     * @param origem the origem to set
     */
    public void setOrigem(Campus origem) {
        this.origem = origem;
    }

    /**
     * @return the dataHoraSaida
     */
    public Date getDataHoraSaida() {
        return dataHoraSaida;
    }

    /**
     * @param dataHoraSaida the dataHoraSaida to set
     */
    public void setDataHoraSaida(Date dataHoraSaida) {
        this.dataHoraSaida = dataHoraSaida;
    }

    /**
     * @return the distancia
     */
    public float getDistancia() {
        return distancia;
    }

    /**
     * @param distancia the distancia to set
     */
    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }
    
    public abstract Date getDataHoraRetorno();
    
    public abstract String getDestino();
}
