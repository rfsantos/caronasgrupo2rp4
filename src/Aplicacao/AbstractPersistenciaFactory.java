/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Aplicacao;

import Repositorio.RepositorioMotorista;
import Repositorio.RepositorioVeiculo;
import Repositorio.RepositorioViagemComum;
import Repositorio.RepositorioViagemFretada;

/**
 *
 * @author rafael
 */
public abstract class AbstractPersistenciaFactory {
    
    public abstract RepositorioViagemFretada createPersistenciaViagemFretada();
    
    public abstract RepositorioVeiculo createPersistenciaVeiculo();
    
    public abstract RepositorioViagemComum createPersistenciaViagemComum();
    
    public abstract RepositorioMotorista createPersistenciaMotorista();
    
}
