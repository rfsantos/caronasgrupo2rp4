/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Aplicacao;

import Repositorio.RepositorioMotorista;
import Persistencia.ConnectionFactory;
import persistencia.DBViagemFretada;
import Repositorio.RepositorioVeiculo;
import Repositorio.RepositorioViagemComum;
import Repositorio.RepositorioViagemFretada;
import persistencia.DBMotorista;
import persistencia.DBMotorista;
import persistencia.DBVeiculo;
import persistencia.DBViagemComum;
import persistencia.DBViagemFretada;


/**
 *
 * @author rafael
 */
public class PersistenciaFactoryPostgres extends AbstractPersistenciaFactory {

    @Override
    public RepositorioViagemFretada createPersistenciaViagemFretada() {

        return new RepositorioViagemFretada(new DBViagemFretada(ConnectionFactory.createConnectionPostgres()));
    }

    @Override
    public RepositorioVeiculo createPersistenciaVeiculo() {

        return new RepositorioVeiculo(new DBVeiculo(ConnectionFactory.createConnectionPostgres()));
    }

    @Override
    public RepositorioViagemComum createPersistenciaViagemComum() {
        
        return new RepositorioViagemComum(new DBViagemComum(ConnectionFactory.createConnectionPostgres()));
    }
    
    public RepositorioMotorista createPersistenciaMotorista(){
        
        return new RepositorioMotorista(new DBMotorista(ConnectionFactory.createConnectionPostgres()));
    }
    
    
}
