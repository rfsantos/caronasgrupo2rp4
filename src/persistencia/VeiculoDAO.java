/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.Veiculo;
import Dominio.ViagemFretada;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Ricardo Burg Machado
 */
public interface VeiculoDAO {

    public boolean incluir(Veiculo veiculo);

    public boolean alterar(Veiculo veiculo);

    public boolean desativar(Veiculo veiculo);

    public Collection<Veiculo> listar(Campus campus);

    public int consultaPlaca(String placa);

    public int buscaQuantProxViagens(String placa);

    public Veiculo buscaVeiculo(String placa);
    
    
}
