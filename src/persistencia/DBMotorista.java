/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.CategoriaCNH;
import Dominio.Motorista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBMotorista implements MotoristaDAO{

    private Connection connection;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private static final String TABELA = "Motorista";
    
    public DBMotorista(Connection con) {
        
        this.connection = con;
    }

      @Override
    public boolean incluirMotorista (Motorista m){
       
        try{
            statement = this.connection.createStatement();
            
            //
// INSERT INTO motorista (cpf,nome,email,cnh,data_nascimento,data_contrat,campus,usuario, categoria_cnh, ativo) 
//	VALUES ('11111111111','Maria dos Santos','maria@gmail.com','22222222222','01.01.1980','25.01.2009', 'Alegrete','maria', 'B', 'true');

            
            System.out.println("cpf: "+m.getCpf()+""
                    + " \nnome: "+m.getNome()+""
                    + " \nemail: "+m.getEmail()+""
                    + " \ncnh: "+m.getCnh()+""
                    + " \nnascimento: "+m.getNascimento()+""
                    + " \ncontratação: "+m.getContratacao()+""
                    + " \ncampus: "+m.getCampus()+""
                    + " \nusuario: "+m.getUsuario()+""
                    + " \ncat cnh: "+m.getCategoriaCNH() +"");
            
            String query = "INSERT INTO " + TABELA + " "
                    +"(cpf, nome, email, cnh, data_nascimento, data_contrat, campus, usuario, categoria_cnh, ativo) values ( "
                     +"'"+m.getCpf()+"',"
                    +"'"+m.getNome()+"',"
                     +"'"+m.getEmail()+"',"
                      +"'"+m.getCnh()+"',"
                    +"'"+m.getNascimento()+"',"
                      +"'"+m.getContratacao()+"',"
                    +"'"+m.getCampus()+"',"
                    +"'"+m.getUsuario()+"',"
                     +"'"+m.getCategoriaCNH()+"',"  
                    +"'TRUE'"
                    +")";
            System.out.println("QUERY: "+query);
            
            if(statement.execute(query)){
                return false;
            }else{
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
    }
    
    /**
     * Metodo responsavel para alterar dados de motorista
     * @param m
     * @return 
     */
    @Override
    public boolean alterarMotorista(Motorista m){
       
        try{
            statement = this.connection.createStatement();
            
            StringBuilder query = new StringBuilder();
            query.append("UPDATE "+ TABELA + "SET");
            query.append("nome = '"+m.getNome()+"'");
            query.append("usuario = '"+m.getUsuario()+"'");
            query.append("senha = '"+m.getSenha()+"'");
            query.append("email = '"+m.getEmail()+"'");
            query.append("cnh = '"+m.getCnh()+"'");
            query.append("nascimento = '"+m.getNascimento()+"'");
            query.append("contratacao = '"+m.getContratacao()+"'");
            
            System.out.println("QUERY: "+query);
            
            if(statement.execute(query.toString())){
                return false;
            }else{
                return true;
            }
            
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    /**
     * Metodo responsavel para buscas um motorista pelo cpf
     * @param cpf
     * @return 
     */
    @Override
    public Motorista buscaMotorista(String cpf) {
        
        String sqlBuscaMotorista = "select * from motorista natural join usuario where cpf = ?";
        PreparedStatement stmt;
        
        try{
            
            stmt = connection.prepareStatement(sqlBuscaMotorista);
            stmt.setString(1, cpf);
        }catch(SQLException sqle){
            
            return null;
        }
        
        try{
            
            return executaConsulta(stmt).get(1);
        }catch(IndexOutOfBoundsException ex){
            
            return null;
        }
    }
    
    
    /**
     * Metodo reponsavel para desativar motorista
     * @param m
     * @return 
     */
    public boolean desativarMotorista (Motorista m){
        
        try{
            statement = this.connection.createStatement();
            
            StringBuilder query = new StringBuilder();
            query.append("UPDATE "+TABELA +"SET ");
            query.append("status = FALSE");
            query.append(" WHERE cpf = '"+m.getCpf()+"'");
            
            System.out.println("QUERY: "+query);
            
            if(statement.execute(query.toString())){
                return false;
            }else{
                return true;
            }
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
        
        
        
    }
    
    /**
     * Metodo responsavel para listar motoristas
     * @param campus
     * @return 
     */
    private List<Motorista> executaConsulta(PreparedStatement stmt){
        
        ResultSet result;
        List<Motorista> motoristas = new ArrayList<Motorista>();
        
        try{
            
            result = stmt.executeQuery();
            
            while(result.next()){
                
                Motorista motorista = new Motorista();
                motorista.setCampus(Campus.fromString(result.getString("camppus")));
                motorista.setCategoriaCNH(CategoriaCNH.fromString(result.getString("categoria_cnh")));
                motorista.setContratacao(new Date(result.getDate("data_contrat").getTime()));
                motorista.setCpf(result.getString("cpf"));
                motorista.setEmail(result.getString("email"));
                motorista.setNascimento(new Date(result.getDate("data_nascimento").getTime()));
                motorista.setNome(result.getString("nome"));
                motorista.setSenha(result.getString("senha"));
                motorista.setUsuario(result.getString("usuario"));
                motoristas.add(motorista);
            }
        }catch(SQLException sqle){
            
            return motoristas;
        }
        
        return motoristas;
    }

    /**
     * Metodo que verifica a quantidade de viagens agendadas para o motorista
     * @param placa
     * @return 
     */
     public int buscaQtdViagens(String cpf) {

        try {
            statement = this.connection.createStatement();

            String query = "SELECT COUNT(*) FROM viagem "
                    + "WHERE cpf = '" + cpf + "' "
                    + "AND data_hora_saida >= CURRENT_DATE";

            System.out.println("QUERY: " + query);

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            int retorno = resultSet.getInt(1);

            System.out.println("QTD PROX VIAGENS: " + retorno);
            return retorno;

        } catch (SQLException ex) {
            System.out.println("EXCEPTION: " + ex);
            return 1;
        }
    }
     
     /**
      * Metodo que verifica se existe alguma viagem pendente
      * para determinado motorista
      * @param cpf
      * @return 
      */
        public boolean viajemPendMotorista(String cpf){
              try {
            statement = this.connection.createStatement();

            String query = "SELECT COUNT(*) FROM viagem "
                    + "WHERE cpf = '" + cpf + "' "
                    + "AND data_hora_saida >= CURRENT_DATE";

            System.out.println("QUERY: " + query);

            ResultSet resultSet = statement.executeQuery(query);
        
            return true;

            } catch (SQLException ex) {
                System.out.println("EXCEPTION: " + ex);
                return false;
            }
        }

    @Override
    public boolean desativarMotorista(String cpf) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List listarMotoristas(Campus c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
