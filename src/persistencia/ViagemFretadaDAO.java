/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Campus;
import Dominio.Viagem;
import Dominio.ViagemFretada;
import Excecoes.PersistenciaException;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author rafael
 */
public interface ViagemFretadaDAO {
    
    public void incluir(ViagemFretada viagem) throws PersistenciaException;

    public void alterar(String placaVeiculo, Date dataHoraSaida, ViagemFretada viagem) throws PersistenciaException;

    public void excluir(String placaVeiculo, Date dataHoraSaida) throws PersistenciaException;

    public Collection<ViagemFretada> listar(Date inicioPeriodo, Date fimPeriodo, Campus origem) throws PersistenciaException;

    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException;
    
    public Collection<Viagem> proximasViagensMotorista (String cpfMotorista) throws PersistenciaException;
}
