/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import Dominio.Viagem;
import Excecoes.PersistenciaException;
import java.util.Collection;

/**
 *
 * @author rafael
 */
public interface ViagemComumDAO {
    
    public Collection<Viagem> proximasViagensVeiculo(String placaVeiculo) throws PersistenciaException;
    
    public Collection<Viagem> proximasViagensMotorista(String cpfMotorista) throws PersistenciaException;
}
