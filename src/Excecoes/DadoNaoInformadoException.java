/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Excecoes;

/**
 *
 * @author rafael
 */
public class DadoNaoInformadoException extends RuntimeException {
    
    private Object dado;
    
    public DadoNaoInformadoException(String message, Object dado){
        
        super(message);
        this.dado = dado;
    }
    
    public Object getDadoNaoInformado(){
        
        return dado;
    }
}
