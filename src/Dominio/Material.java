/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;


public class Material {

    private String material;

    public Material(){}
    
    public Material(String material){
        setMaterial(material);
    }
    
    /**
     * @return the material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(String material) {
        this.material = material;
    }
    
}
