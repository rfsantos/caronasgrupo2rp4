/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import Persistencia.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author NoteCCE
 */
public class Verificacoes {
    
    /**
     * Verifica se a idade for menor que 18 anos
     * @param dataNasc
     * @return 
     */
    public static boolean verDataNasc(Date nascimento){
        
        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.YEAR, -18);
        
        if(nascimento.after(cal.getTime())){
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Verifica se o ano for menor que 18 anos
     * @param dataNasc
     * @return 
     */
    public static boolean verDataCont(Date contratacao){
        
        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.YEAR, -18);
        
        if(contratacao.after(cal.getTime())){
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Método para verificar se o CPF é válido
     * @param cpfNum
     * @return 
     */
     public static boolean VerCpfValido(String cpfNum){
        int[] cpf = new int[cpfNum.length()]; //define o valor com o tamanho da string  
        int resultP = 0;  
        int resultS = 0;  
  
        //converte a string para um array de integer  
        for (int i = 0; i < cpf.length; i++) {  
            cpf[i] = Integer.parseInt(cpfNum.substring(i, i + 1));  
        }  
  
        //calcula o primeiro número(DIV) do cpf  
        for (int i = 0; i < 9; i++) {  
            resultP += cpf[i] * (i + 1);  
        }  
        int divP = resultP % 11;  
  
        //se o resultado for diferente ao 10º digito do cpf retorna falso  
        if (divP != cpf[9]) {  
            return false;  
        } else {  
            //calcula o segundo número(DIV) do cpf  
            for (int i = 0; i < 10; i++) {  
                resultS += cpf[i] * (i);  
            }  
            int divS = resultS % 11;  
  
            //se o resultado for diferente ao 11º digito do cpf retorna falso  
            if (divS != cpf[10]) {
                return false;
            }
    }
        return false;
    }
    
    
    /**
    
    public static boolean verData(final String data){
             
        if (data != null) {  
            try {  
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
                df.setLenient(false);  
                java.util.Date d = df.parse(data);  
                return true;  
            } catch (ParseException e) {  
                System.out.println("isValidDate" + "ParseException=" + e.getMessage());  
            }  
        }  
        return false;  
    }  
   public static String formatarData(String date) {    
      DateFormat df = new SimpleDateFormat("dd/MM/yyyy");    
   try {  
      Date data = df.parse(date);  
      return df.format(data);    
   } catch (ParseException e) {  
   }  
   return null;   
   }  

    **/
    
  
    /**
     * Metodo que ferifica se numero de cpf já existe
     * @param cpf
     * @return 
     */
    public static boolean CpfExiste(String cpf) throws SQLException{
        
    Connection con = ConnectionFactory.createConnectionPostgres();      
    String vSQL = "select * from motorista where cpf";       
    PreparedStatement com = (PreparedStatement) con.prepareStatement(vSQL);       
    com.setString(1, cpf);      
    ResultSet rs = com.executeQuery();       
    Motorista m = null;    
    
    if (rs.next()) {    
    //    m = new Motorista();    
            
    //    m.setNome(rs.getString("usuario"));
        return true;
    }    
    return false;       
    }
    
    
    /**
     * Método que verifica se o formato de email é valido
     * @param email
     * @return 
     */
    public static boolean VerEmail(String email){
        
        if(email !=null && email.length()>0){
            String expression = "^[\\w\\.-]+@(\\w\\-]+[a-z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()){
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Método que verifica se o nome de usuario já esta sendo utilizado
     * @param usuario
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public static boolean VerUsuarioExiste(String usuario) throws SQLException, ClassNotFoundException{       
     
    Connection con = ConnectionFactory.createConnectionPostgres();      
    String vSQL = "select * from motorista where usuario";       
    PreparedStatement com = (PreparedStatement) con.prepareStatement(vSQL);       
    com.setString(1, usuario);      
    ResultSet rs = com.executeQuery();       
    Motorista m = null;    
    
    if (rs.next()) {    
    //    m = new Motorista();    
            
    //    m.setNome(rs.getString("usuario"));
        return true;
    }    
    return false;   
    }
   /** 
    * Metodo que verifica se a senha e forte
    * 
    public static boolean VerSenhaForte(String senha){
    
    int lengthSenha = senha.length();
    if (lengthSenha > = 10) {
        return false;
    }

    Boolean hasCapital = false;
    Boolean hasSmallLetter = false;
    Boolean hasDigit = false;
    for (int i = 0; i < senha.length();="" i++)="" ="">
    char ch = senha.charAt(i);
    if {
        (Character.isUpperCase(ch))
        hasCapital = true;
    }
    if {
        (Character.isLowerCase(ch))
        hasSmallLetter = true;
    }
    if {
        (Character.isDigit(ch))
        hasDigit = true;
    }
    }
    return hasCapital && hasSmallLetter && hasDigit;

   //     return false;
    }
    
  **/
}

