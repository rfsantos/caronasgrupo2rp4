/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import java.util.Date;

/**
 *
 * @author rafael
 */
public class ViagemFretada extends Viagem {

    private String responsavel;
    private String CPFResponsavel;
    private String destino;
    private Date dataHoraRetorno;
    private int numPassageiros;
    private String motivo;
    private float duracao;
    
    public ViagemFretada(Veiculo veiculo, Motorista motorista, Campus origem, Date dataHoraSaida, float distancia) {

        super(veiculo, motorista, origem, dataHoraSaida, distancia);
    }

    /**
     * @return the responsavel
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the CPFResponsavel
     */
    public String getCPFResponsavel() {
        return CPFResponsavel;
    }

    /**
     * @param CPFResponsavel the CPFResponsavel to set
     */
    public void setCPFResponsavel(String CPFResponsavel) {
        this.CPFResponsavel = CPFResponsavel;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the dataHoraRetorno
     */
    public Date getDataHoraRetorno() {
        return dataHoraRetorno;
    }

    /**
     * @param dataHoraRetorno the dataHoraRetorno to set
     */
    public void setDataHoraRetorno(Date dataHoraRetorno) {
        this.dataHoraRetorno = dataHoraRetorno;
    }

    /**
     * @return the numPassageiros
     */
    public int getNumPassageiros() {
        return numPassageiros;
    }

    /**
     * @param numPassageiros the numPassageiros to set
     */
    public void setNumPassageiros(int numPassageiros) {
        this.numPassageiros = numPassageiros;
    }

    /**
     * @return the motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * @param motivo the motivo to set
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * @return the duracao
     */
    public float getDuracao() {
        return duracao;
    }

    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(float duracao) {
        this.duracao = duracao;
    }
    
}
