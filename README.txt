************INSTALA��O E CONFIGURA��O DO BANCO DE DADOS************

SGBD utilizado: PostgreSQL
USU�RIO:        postgres
SENHA:          unipampa
NOME DO BANCO DE DADOS: Caronas

************CRIA��O DA ESTRUTURA*****************
Para a cria��o da estrutura do banco de dados existe o arquivo ScriptDBCaronas3.sql (que est� no diret�rio Config), que ao ser executado no SGBD  faz a cria��o de toda a estrutura.

************INSER��O DE DADOS FICTICIOS*****************
Para o povoamento do banco de dados com dados fict�cios existe o arquivo cadastroTudo2.sql (que est� no diret�rio Config).


*********** DRIVER DE CONEX�O************
Para estabilizar a conex�o do sistema com banco de dados, deve-se importar em bibliotecas o Driver JDBC do PostgreSQL.

