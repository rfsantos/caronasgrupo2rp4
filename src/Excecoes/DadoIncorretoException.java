/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Excecoes;

/**
 *
 * @author rafael
 */
public class DadoIncorretoException extends RuntimeException{

    private Object dado;
    
    public DadoIncorretoException(String message, Object dado){
        
        super(message);
        this.dado = dado;
    }
    
    public Object getDadoIncorreto(){
        
        return dado;
    }
}
