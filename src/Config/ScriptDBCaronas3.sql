
CREATE SEQUENCE modo_acesso_cod_modo_acesso_seq;

CREATE TABLE modo_acesso (
                cod_modo_acesso INTEGER NOT NULL DEFAULT nextval('modo_acesso_cod_modo_acesso_seq'),
                descricao VARCHAR(30) NOT NULL,
                CONSTRAINT codmodoacesso_modoacesso PRIMARY KEY (cod_modo_acesso)
);


ALTER SEQUENCE modo_acesso_cod_modo_acesso_seq OWNED BY modo_acesso.cod_modo_acesso;

CREATE TABLE Login (
                usuario VARCHAR(40) NOT NULL,
                senha VARCHAR(20) NOT NULL,
                cod_modo_acesso INTEGER NOT NULL,
                CONSTRAINT usuario_login PRIMARY KEY (usuario)
);


CREATE TABLE Caronista (
                matricula VARCHAR(9) NOT NULL,
                cpf VARCHAR(11) NOT NULL,
                nome VARCHAR(40) NOT NULL,
                email VARCHAR(40) NOT NULL,
                data_nasc DATE NOT NULL,
                usuario VARCHAR(40) NOT NULL,
                CONSTRAINT matricula_caronista PRIMARY KEY (matricula)
);


CREATE TABLE Veiculo (
                placa VARCHAR(8) NOT NULL,
                marca VARCHAR(30) NOT NULL,
                modelo VARCHAR(30) NOT NULL,
                ano INTEGER NOT NULL,
                capacidade INTEGER NOT NULL,
                quilometragem BIGINT NOT NULL,
                campus VARCHAR(30) NOT NULL,
                cor VARCHAR(15) NOT NULL,
                categoria_cnh VARCHAR(2) NOT NULL,
                ativo BOOLEAN NOT NULL,
                CONSTRAINT placa_veiculo PRIMARY KEY (placa)
);


CREATE TABLE Motorista (
                cpf VARCHAR(11) NOT NULL,
                nome VARCHAR(40) NOT NULL,
                email VARCHAR(50) NOT NULL,
                cnh VARCHAR(11) NOT NULL,
                data_nascimento DATE NOT NULL,
                categoria_cnh VARCHAR(2) NOT NULL,
                data_contrat DATE NOT NULL,
                campus VARCHAR(30) NOT NULL,
                usuario VARCHAR(40) NOT NULL,
                ativo BOOLEAN NOT NULL,
                CONSTRAINT cpf_motorista PRIMARY KEY (cpf)
);


CREATE TABLE Viagem (
                data_hora_saida TIMESTAMP NOT NULL,
                placa VARCHAR(8) NOT NULL,
                distancia REAL NOT NULL,
                cpf VARCHAR(11) NOT NULL,
                origem VARCHAR(30) NOT NULL,
                CONSTRAINT data_hora_placa_viagem PRIMARY KEY (data_hora_saida, placa)
);


CREATE TABLE Viagem_Comum (
                data_hora_saida TIMESTAMP NOT NULL,
                placa VARCHAR(8) NOT NULL,
                material VARCHAR(40),
                destino VARCHAR(30) NOT NULL,
                data_hora_saida_retorno TIMESTAMP,
                placa_retorno VARCHAR(8),
                CONSTRAINT data_hora_placa_viagemcomum PRIMARY KEY (data_hora_saida, placa)
);


CREATE TABLE carona (
                data_hora_saida TIMESTAMP NOT NULL,
                placa VARCHAR(8) NOT NULL,
                matricula VARCHAR(9) NOT NULL,
                CONSTRAINT viagem_carona PRIMARY KEY (data_hora_saida, placa, matricula)
);


CREATE TABLE Viagem_Fretada (
                data_hora_saida TIMESTAMP NOT NULL,
                placa VARCHAR(8) NOT NULL,
                responsavel VARCHAR(50) NOT NULL,
                num_passageiros INTEGER NOT NULL,
                motivo VARCHAR(40) NOT NULL,
                destino VARCHAR(30) NOT NULL,
                cpf_responsavel VARCHAR(11) NOT NULL,
                duracao REAL NOT NULL,
                data_hora_retorno TIMESTAMP NOT NULL,
                CONSTRAINT data_hora_placa_viagemfretada PRIMARY KEY (data_hora_saida, placa)
);


ALTER TABLE Login ADD CONSTRAINT modo_acesso_login_fk
FOREIGN KEY (cod_modo_acesso)
REFERENCES modo_acesso (cod_modo_acesso)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Caronista ADD CONSTRAINT login_caronista_fk
FOREIGN KEY (usuario)
REFERENCES Login (usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Motorista ADD CONSTRAINT login_motorista_fk
FOREIGN KEY (usuario)
REFERENCES Login (usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carona ADD CONSTRAINT caronista_carona_fk
FOREIGN KEY (matricula)
REFERENCES Caronista (matricula)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Viagem ADD CONSTRAINT viagem_veiculo_fk
FOREIGN KEY (placa)
REFERENCES Veiculo (placa)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Viagem ADD CONSTRAINT motorista_viagem_fk
FOREIGN KEY (cpf)
REFERENCES Motorista (cpf)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE Viagem_Fretada ADD CONSTRAINT viagemfretada_viagem_fk
FOREIGN KEY (placa, data_hora_saida)
REFERENCES Viagem (placa, data_hora_saida)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE Viagem_Comum ADD CONSTRAINT viagemcomum_viagem_fk
FOREIGN KEY (placa, data_hora_saida)
REFERENCES Viagem (placa, data_hora_saida)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE carona ADD CONSTRAINT viagem_comum_carona_fk
FOREIGN KEY (placa, data_hora_saida)
REFERENCES Viagem_Comum (placa, data_hora_saida)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE Viagem_Comum ADD CONSTRAINT viagem_comum_viagem_comum_fk
FOREIGN KEY (placa_retorno, data_hora_saida_retorno)
REFERENCES Viagem_Comum (placa, data_hora_saida)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;
